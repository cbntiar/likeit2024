<?php

namespace app\controllers;

use app\components\Helpers;
use app\models\BelumKirim;
use app\models\MContent;
use app\models\MMailTemplate;
use app\models\TCertificateRequest;
use app\models\TQuestion;
use app\models\TQuiz;
use app\models\TRegistration;
use app\models\TRegistrationSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Da\QrCode\QrCode;
use Da\QrCode\Format\BookmarkFormat;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use Mpdf\Mpdf;

// use ImageKit\ImageKit;  
/**
 * EventController implements the CRUD actions for TRegistration model.
 */

// const SCRIPT_URL = 'https://script.google.com/macros/s/AKfycbwdw55aQVKo3y_HJ4qkuYJP-brLVZYF5KrPxRICQz-1-XC6lTkfahy6MpybsWrOqqzWbw/exec';
const SCRIPT_URL = 'https://script.google.com/macros/s/AKfycby8xeTzayr-GwJyQcE1RSzhqW3xsvoolQR2bmAaAiHMJofBzLXEuTpQMpIBAIvJsnXxKg/exec';


class EventController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TRegistration models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TRegistrationSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TRegistration model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TRegistration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TRegistration();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionRegistrasi($direct_invite = 0)
    {
      

        $model = new TRegistration();

        if ($this->request->isPost) {
            $post = $this->request->post();
            $now = date('Y-m-d H:i:s');
            $model->created_at = $now;

            if ($model->load($post) && $model->save()) {

                $body = $post['TRegistration'];
                $body['tanggal'] = $now;

                // $this->postToGoogleSheet('https://script.google.com/macros/s/AKfycbwlq8T_5uX0KepWkG1sLex3FyX05vWNDO1z1o0ZuThosvSyrAggSSvgp-l4weDvu6MO/exec', $body);

                return $this->redirect(['thanks']);
            }
        } else {
            $model->loadDefaultValues();
        }

        $check = MContent::find()->where(['type' => 'form_registrasi'])->one();

        if ($check->content == 'coming_soon') {
            return $this->render('coming_soon', []);
        } else if ($check->content == 'closed') {
            return $this->render('closed', [
                'type' => 'Pendaftaran'
            ]);
        } else if ($check->content == 'open') {
            return $this->render('registrasi', [
                'model' => $model,
                'direct_invite' => $direct_invite
            ]);
        }
    }

    public function actionSendRegistration()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        if ($post) {
            $model = new TRegistration();
            $body = $post['TRegistration'];

            $check = TRegistration::find()->where(['no_ktp' => $body['no_ktp']])->one();
            if (!empty($check)) {
                // return [
                //     'success' => false,
                //     'message' => 'Nomor KTP sudah terdaftar.'
                // ];
            }

            $now = date('Y-m-d H:i:s');
            $model->load($post);
            $model->created_at = $now;
            $model->nama_lengkap = encrypt($body['nama_lengkap']);
            $model->email = encrypt($body['email']);
            $model->no_hp = encrypt($body['no_hp']);

            if ($model->save(false)) {

                $direct_invite = $post['direct_invite'];

                if ($direct_invite == '1') {

                    if (!empty($model)) {
                        $template = MMailTemplate::find()->where(['type' => 'undangan'])->one();
        
                        if (filter_var(decrypt($model->email), FILTER_VALIDATE_EMAIL)) {
                            $qrCode = (new QrCode('https://likeit.co.id/web/index.php/event/konfirmasi?e=' . $model->email))
                                ->setSize(250)
                                ->setMargin(5)
                                ->useForegroundColor(3, 39, 144);
        
                            $dir = dirname(__DIR__, 1) . '/web/images/code.png';
        
                            $qrCode->writeFile($dir);
        
                            $upload = $this->uploadImageKit($model->email);
        
                            if (!empty($upload)) {
                                $newTemplate = str_replace(['{nama_lengkap}', '{qr_code}'], [decrypt($model->nama_lengkap), $upload->url], $template->template);
                                $subject = $template->subject;
                                
        
                                $send = Yii::$app->mailer->compose()
                                    ->setFrom('no-reply@likeit.co.id')
                                    ->setTo(decrypt($model->email))
                                    ->setSubject($subject)
                                    ->setHtmlBody($newTemplate)
                                    ->send();
        
                                if ($send) {
        
                                    $model->status = 2;
                                    $model->updated_at = date('Y-m-d H:i:s');
                                    $model->save(false);
                                }
                            }
                        }
                    }
                }

                // $body = $post['TRegistration'];
                $body['tanggal'] = $now;

                // $this->postToGoogleSheet(SCRIPT_URL . '?sheet_name=Registrasi', $body);

                $data = [
                    'success' => true,
                ];
            } else {
                $data = [
                    'success' => false,
                    'message' => 'Gagal mengirim data pendaftaran, mohon coba beberapa saat lagi.'
                ];
            }

            return $data;
        }
    }

    public function actionCertificateRequest()
    {
        return $this->render('closed', []);

        $model = new TCertificateRequest();

        return $this->render('certificate_request', [
            'model' => $model,
        ]);
    }

    public function actionSendCertificateRequest()
    {
        set_time_limit(0);
        session_write_close();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        $foto = UploadedFile::getInstanceByName('TCertificateRequest[image]');

        if ($post) {
            $model = new TCertificateRequest();
            $body = $post['TCertificateRequest'];

            if ($foto->size > 2000000) {
                return $data = [
                    'success' => false,
                    'message' => "File melebihi ukuran maksimum 2Mb.",
                ];
            }

            if ($foto->type == 'image/png' || $foto->type == 'image/jpeg') {
                $upload = $this->uploadImageKit($body['email'], 'certificate_request', $foto->tempName);
                if (!empty($upload)) {
                    $now = date('Y-m-d H:i:s');
                    $body = $post['TCertificateRequest'];

                    $model->load($post);
                    $model->created_at = $now;
                    $model->image = $upload->url;
                    $model->nama_lengkap = encrypt($body['nama_lengkap']);
                    $model->email = encrypt($body['email']);
                    $model->no_hp = encrypt($body['no_hp']);
                    
                    if ($model->save(false)) {

                        $body['tanggal'] = $now;
                        $body['image'] = $upload->url;

                        // $this->postToGoogleSheet(SCRIPT_URL . '?sheet_name=Sertifikat', $body);

                        $data = [
                            'success' => true,
                            'message' => "Request berhasil, permintaan akan kami proses.",
                        ];
                    } else {
                        $data = [
                            'success' => false,
                            'message' => "Gagal melakukan request sertifikat, mohon coba beberapa saat lagi.",
                        ];
                    }
                } else {
                    $data = [
                        'success' => false,
                        'message' => "Gagal melakukan upload data",
                    ];
                }
            } else {
                $data = [
                    'success' => false,
                    'message' => "Format file tidak sesuai.",
                ];
            }

            return $data;
        }
    }

    public function actionSaveCertificateRequest()
    {
        set_time_limit(0);
        session_write_close();

        $success = 0;
        
        $list = BelumKirim::find()->where((['status_registrasi' => 'registrasi']))->all();

        foreach ($list as $key => $value) {
            $now = date('Y-m-d H:i:s');
            $model = new TCertificateRequest();

            $model->created_at = $now;
            $model->event_id = 1;
            $model->no_hp = "";
            $model->image = "";
            $model->nama_lengkap = encrypt($value->nama_lengkap);
            $model->email = encrypt($value->email);
            // $model->no_hp = encrypt($value->no_hp);
            $model->instansi = $value->instansi;
            
            if ($model->save(false)) {
                $success++;
            }
        }

        echo $success;
    }

    public function actionKonfirmasi($e = "", $self = '0')
    {
        $message = '';
        $success = false;
        $model = null;

        // if (date('Y-m-d') != '14-08-2023') {
        //     return $this->render('konfirmasi', [
        //         'model' => $model,
        //         'self' => $self,
        //         'success' => false,
        //         'message' => 'Konfirmasi dapat dilakukan pada tanggal 14 Agustus 2023',
        //     ]);
        // }

        if ($e != "") {
            // $email = Helpers::decrypt(rawurldecode($e));
            $email = $e;

            if ($email == false) {
                $success = false;
                $message = 'Data tidak valid';
            } else {
                $model = TRegistration::find()->where('(email = :e OR no_ktp = :e) and (status = 1 OR status = 2 OR status = 3)', [
                    'e' => $email
                ])->one();


                if (!empty($model)) {
                    if ($model->status == 3) {
                        $success = false;
                        $message = 'Peserta sudah melakukan konfirmasi pada : ' . $model->updated_at;
                    } else {
                        $model->status = 3;
                        $model->updated_at = date('Y-m-d H:i:s');
                        if ($model->save(false)) {
                            $success = true;
                            $message = 'Konfirmasi kehadiran berhasil';
                        }
                    }
                } else {
                    $success = false;
                    $message = 'Data peserta tidak ditemukan';
                }
            }

            return $this->render('konfirmasi', [
                'model' => $model,
                'self' => $self,
                'success' => $success,
                'message' => $message,
            ]);
        } else {
            return $this->render('konfirmasi', [
                'self' => $self,
                'success' => $success,
                'model' => $model,
                'message' => $message,
            ]);
        }
    }

    public function actionKonfirmasiOtomatis()
    {
      
        $message = '';
        $success = false;
        $model = null;

        // if (date('Y-m-d') != '14-08-2023') {
        //     return $this->render('konfirmasi', [
        //         'model' => $model,
        //         'self' => $self,
        //         'success' => false,
        //         'message' => 'Konfirmasi dapat dilakukan pada tanggal 14 Agustus 2023',
        //     ]);
        // }
        $email = $_POST['email'];

        if (!empty($email)) {
                // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $model = TRegistration::find()->where('(email = :e OR no_ktp = :e) and (status = 1 OR status = 2 OR status = 3)', [
                    'e' => $email
                ])->one();

                if (!empty($model)) {
                    if ($model->status != 3) {
                        $model->status = 3;
                        $model->updated_at = date('Y-m-d H:i:s');
                        if ($model->save(false)) {
                            $success = true;
                            $message = 'Konfirmasi kehadiran berhasil';
                        }
                    } else {
                        $success = false;
                        $message = 'Peserta sudah melakukan konfirmasi pada : ' . $model->updated_at;
                    }
                } else {
                    $success = false;
                    $message = 'Data peserta tidak ditemukan';
                }

                return $this->renderPartial('konfirmasi_otomatis_result', [
                    'model' => $model, 'success' => $success, 'message' => $message
                ]);

        } else {
            
        }

        return $this->render('konfirmasi_otomatis', [
           
        ]);
    }

    public function actionQuiz()
    {
        // return $this->render('coming_soon', []);

        // if (date('Y-m-d H:i') == '2024-11-06 14:15') {
        //     return $this->render('closed', [
        //         'type' => 'Game password'
        //     ]);
        // }
        return $this->render('closed', [
            'type' => 'Game password'
        ]);

        // return $this->render('coming_soon', []);

        $model = new TQuiz();

        return $this->render('quiz', [
            'model' => $model,
        ]);
    }

    public function actionQuestion()
    {

        return $this->render('closed', [
            'type' => 'Pengajuan pertanyaan'
        ]);

        // return $this->render('coming_soon', []);

        $model = new TQuestion();

        return $this->render('question', [
            'model' => $model,
        ]);

        if ($check->content == 'coming_soon') {
            return $this->render('coming_soon', []);
        } else if ($check->content == 'closed') {
            return $this->render('closed', [
                'type' => 'Pengajuan pertanyaan'
            ]);
        } else if ($check->content == 'open' || date("Y-m-d") == '2023-08-14') {
            return $this->render('question', [
                'model' => $model,
            ]);
        }
    }

    public function actionSendQuestion()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        if ($post) {
            $now = date('Y-m-d H:i:s');

            $model = new TQuestion();
            $model->created_at = $now;

            $body = $post['TQuestion'];
            $model->load($post);

            $model->nama_lengkap = encrypt($body['nama_lengkap']);
            $model->email = encrypt($body['email']);
            $model->no_hp = encrypt($body['no_hp']);

            if ($model->save(false)) {

                $body = $post['TQuestion'];
                $body['tanggal'] = $now;

                // $this->postToGoogleSheet(SCRIPT_URL . '?sheet_name=Pertanyaan', $body);

                $data = [
                    'success' => true,
                ];
            } else {
                $data = [
                    'success' => false,
                ];
            }

            return $data;
        }
    }

    public function actionSendQuiz()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        if ($post) {
            $now = date('Y-m-d H:i:s');

            $model = new TQuiz();
            $model->created_at = $now;

            $body = $post['TQuiz'];
            $model->load($post);

            $model->nama_lengkap = encrypt($body['nama_lengkap']);
            $model->email = encrypt($body['email']);
            $model->no_hp = encrypt($body['no_hp']);

            if ($model->save(false)) {

                $body = $post['TQuiz'];
                $body['tanggal'] = $now;

                // $this->postToGoogleSheet(SCRIPT_URL . '?sheet_name=Pertanyaan', $body);

                $data = [
                    'success' => true,
                ];
            } else {
                $data = [
                    'success' => false,
                ];
            }

            return $data;
        }
    }

    public function postToGoogleSheet($url, $body)
    {
        try {

            $options = [
                'http' => [
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => http_build_query($body),
                ],
            ];
            

            $context = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === false) {
                throw 'Error';
            }
            // $curl = curl_init();

            // curl_setopt_array($curl, array(
            //     CURLOPT_URL => $url,
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_ENCODING => '',
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 0,
            //     CURLOPT_FOLLOWLOCATION => true,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => 'POST',
            //     CURLOPT_POSTFIELDS => http_build_query($body),
            //     CURLOPT_HTTPHEADER => array(
            //         'Content-Type: application/json',
            //         'Content-Length: ' . strlen(http_build_query($body))      
            //     ),
            // ));

            // $response = curl_exec($curl);

            // var_dump($response);exit;

            // curl_close($curl);

            // return $response;

        } catch (\Throwable $e) {

            trigger_error(
                sprintf(
                    'HTTP failed with error #%d: %s',
                    $e->getCode(),
                    $e->getMessage()
                ),
                E_USER_ERROR
            );
        }
    }

    public function actionThanks()
    {
        return $this->render('thanks', []);
    }

    /**
     * Updates an existing TRegistration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TRegistration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TRegistration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return TRegistration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TRegistration::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionBlastUndangan()
    {
        set_time_limit(0);
        session_write_close();

        $list = TRegistration::find()->where(['status' => 1])->all();
        $template = MMailTemplate::find()->where(['type' => 'undangan'])->one();
        $success = 0;

        foreach ($list as $key => $value) {
            if (filter_var($value->email, FILTER_VALIDATE_EMAIL)) {
                $qrCode = (new QrCode('https://likeit.co.id/index.php/event/konfirmasi?e=' . $value->email))
                    ->setSize(250)
                    ->setMargin(5)
                    ->useForegroundColor(3, 39, 144);

                $dir = dirname(__DIR__, 1) . '/web/images/code.png';

                $qrCode->writeFile($dir);

                $upload = $this->uploadImageKit($value->email);

                if (!empty($upload)) {
                    $newTemplate = str_replace(['{nama_lengkap}', '{qr_code}'], [$value->nama_lengkap, $upload->url], $template->template);

                    $send = Yii::$app->mailer->compose()
                        ->setFrom('no-reply@likeit.co.id')
                        ->setTo($value->email)
                        ->setSubject("[REMINDER] " . $template->subject)
                        ->setHtmlBody($newTemplate)
                        ->send();

                    if ($send) {
                        $success++;

                        $value->status = 2;
                        $value->updated_at = date('Y-m-d H:i:s');
                        $value->save(false);
                    }
                }
            }
        }


        var_dump($success);
        exit;
    }

    public function uploadImageKit($email, $folder = 'qr_code', $dir = '')
    {


        try {
            if ($dir == '') {
                $dir = dirname(__DIR__, 1) . '/web/images/code.png';
            }

            $ch = curl_init();

            // Check if initialization had gone wrong*    
            if ($ch === false) {
                throw new \Exception('failed to initialize');
            }

            curl_setopt_array($ch, array(
                CURLOPT_URL => 'https://upload.imagekit.io/api/v1/files/upload',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('file' => new \CURLFILE($dir), 'fileName' => $email . '.png', 'folder' => $folder),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic cHJpdmF0ZV95SjArR1lUaG9hVW84QzhhYktCUDFzL1gzc1U9Og=='
                ),
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
            ));

            $content = curl_exec($ch);

            // Check the return value of curl_exec(), too
            if ($content === false) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            }

            // Check HTTP return code, too; might be something else than 200
            $httpReturnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);

            return json_decode($content);
            /* Process $content here */
        } catch (\Throwable $e) {

            curl_close($ch);

            trigger_error(
                sprintf(
                    'Curl failed with error #%d: %s',
                    $e->getCode(),
                    $e->getMessage()
                ),
                E_USER_ERROR
            );
        }
    }

    public function actionReport()
    {
        // get your HTML raw content without any layouts or scripts
        // $content = $this->renderPartial('_reportView');
        $content = '<img src="https://ik.imagekit.io/d9hiweoihy/banner/IG%20STORY_1920x1080_LIKE%20IT%202023_-01.jpg?updatedAt=1691499511245">';

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Krajee Report Header'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionKirimUndangan($email, $reminder = 0)
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        set_time_limit(0);
        session_write_close();

        try {
            $model = TRegistration::find()->where(['email' => $email])->one();

            if (!empty($model)) {
                $template = MMailTemplate::find()->where(['type' => 'undangan'])->one();
                $success = 0;

                if (filter_var(decrypt($model->email), FILTER_VALIDATE_EMAIL)) {
                    $qrCode = (new QrCode('https://likeit.co.id/web/index.php/event/konfirmasi?e=' . $model->email))
                        ->setSize(250)
                        ->setMargin(5)
                        ->useForegroundColor(3, 39, 144);

                    $dir = dirname(__DIR__, 1) . '/web/images/code.png';

                    $qrCode->writeFile($dir);

                    $upload = $this->uploadImageKit($model->email);

                    if (!empty($upload)) {
                        $newTemplate = str_replace(['{nama_lengkap}', '{qr_code}'], [decrypt($model->nama_lengkap), $upload->url], $template->template);
                        $subject = $template->subject;
                        if ($reminder == 1) {
                            $subject = "REMINDER : " . $template->subject;
                        }

                        $send = Yii::$app->mailer->compose()
                            ->setFrom('no-reply@likeit.co.id')
                            ->setTo(decrypt($model->email))
                            ->setSubject($subject)
                            ->setHtmlBody($newTemplate)
                            ->send();

                        if ($send) {
                            $success++;

                            $model->status = 2;
                            $model->updated_at = date('Y-m-d H:i:s');
                            $model->save(false);
                        } else {
                            throw new \Exception("Gagal mengirim email", 1);
                        }
                    } else {
                        throw new \Exception("Gagal mengupload QR Code", 1);
                    }
                }
            } else {
                throw new \Exception("Data tidak ditemukan", 1);
            }

            return [
                'success' => true,
                'message' => 'Undangan berhasil terkirim',
            ];
        } catch (\Throwable $th) {
            return [
                'success' => false,
                'message' => $th->getMessage(),
            ];
        }
    }

    public function actionTestmail($email)
    {
        try {
            $send = Yii::$app->mailer->compose()
            ->setFrom('no-reply@likeit.co.id')
            ->setTo($email)
            ->setSubject("[REMINDER] test email")
            ->setHtmlBody('test')
            ->send();

            var_dump($send);
        } catch (\Throwable $th) {
            echo $th;
        }
        
    }

    public function actionComingSoon()
    {
        return $this->render('coming_soon', [
        ]);
    }

    public function actionGetCertificate($e_id = '')
    {

        if (isset($e_id)) {
            $model = TCertificateRequest::find()->where(['id' => decrypt($e_id)])->one();

            if (!empty($model)) {

                $pdf = $this->generateCertificatePdf($model->nama_lengkap, $model->instansi);

                return $pdf;

            } else {
                return $this->render('not_found', ['message' => 'Data sertifikat belum tersedia']);
            }
        } else {
            return $this->render('not_found', ['message' => 'Data sertifikat belum tersedia']);
        }
        
    }

    public function actionGetCertificateInput($nama = "", $instansi = "")
    {

        $pdf = $this->generateCertificatePdf(ucfirst($nama), $instansi);

        return $pdf;
        
    }

    function shortenName($fullName) {
        $names = explode(' ', $fullName); // Memecah nama menjadi array
        $shortenedName = [];
        
        // Ambil 3 kata pertama
        for ($i = 0; $i < min(4, count($names)); $i++) {
            $shortenedName[] = $names[$i];
        }
        
        // Ambil huruf pertama dari kata ke-4 dan seterusnya
        for ($i = 4; $i < count($names); $i++) {
            $shortenedName[] = substr($names[$i], 0, 1).'.'; // Ambil huruf pertama
        }
        
        // Gabungkan nama yang sudah diproses
        return implode(' ', $shortenedName);
    }

    private function generateCertificatePdf($nama = "", $instansi = "")
    {
        try {

            $nama = $this->shortenName($nama);


            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];


            $mpdf = new Mpdf([
                'orientation' => 'L',
                'fontDir' => array_merge($fontDirs, [
                    Yii::getAlias('@webroot/fonts/'),
                ]),
                'fontdata' => $fontData + [ 
                    'pinyonscript' => [
                        'R' => 'PinyonScript-Regular.ttf',
                    ],
                    'tt-chocolates' => [
                        'R' => 'TT Chocolates Bold Italic.ttf',
                    ]
                ],

            ]);

            // echo '<pre>';
            // print_r($mpdf->fontdata);
            // echo '</pre>';
            // exit;

           
            $pageCount = $mpdf->setSourceFile(Yii::getAlias('@webroot/ATTENDACE #1 Back.pdf'));
            $templatePage = $mpdf->importPage(1); // Ambil halaman pertama dari template
            $mpdf->useTemplate($templatePage);

           
            $namaStyle = 'font-family: PinyonScript; font-size: 70px; color: #DD4C2A; text-align: center; position: absolute; top: 320px; width: 100%'; 
            $mpdf->WriteFixedPosHTML( "<div style='$namaStyle'>$nama</div>", 0, 0, 500, 500,);

            if ($instansi != '' && $instansi != '-' && $instansi != 'UMUM' && $instansi != 'UMKM') {
                $instansiStyle = 'font-family: TT-Chocolates; font-size: 28px; color: #1E3653; text-align: center; position: absolute; top: 425px; width: 100%';
                $mpdf->WriteFixedPosHTML( "<div style='$instansiStyle'>dari ".strtoupper($instansi)."</div>", 0, 0, 500, 500,);
            }
    
            // $pdfContent = $mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN);

            $filename = 'Sertifikat LIKE IT 2024 - '.str_replace('.', '', $nama).'.pdf';
        
            return $mpdf->Output($filename, \Mpdf\Output\Destination::DOWNLOAD); 
            // return $pdfContent;
        } catch (\Exception $e) {


            var_dump($e->getMessage());
            exit;
            Yii::error("Gagal membuat PDF: " . $e->getMessage());

            return [
                'success' => false,
                'message' => 'Error generate pdf.: '. $e->getMessage(),
                'data' => [
                    "continue" => false
                ],
            ];;
        }
    }
   
     
}
