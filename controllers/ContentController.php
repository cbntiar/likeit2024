<?php

namespace app\controllers;

use app\models\MContent;

class ContentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($type = '')
    {
        $data = MContent::find()->where(['type' => $type])->one();

        return $this->render('view', [
            'data' => $data
        ]);
    }

    public function actionProfile()
    {
        return $this->render('profile');
    }

    public function actionStakeholders()
    {
        return $this->render('stakeholders');
    }

    public function actionSocialMedia()
    {
        return $this->render('social_media');
    }

    public function actionLpsCreavid2022()
    {
        return $this->render('lps_creavid_2022');
    }

    public function actionJingleLikeIt()
    {
        return $this->render('jingle_like_it');
    }

    public function actionLikeIt1()
    {
        return $this->render('like_it_1');
    }

    public function actionLikeIt2()
    {
        return $this->render('like_it_2');
    }

    public function actionLikeIt3()
    {
        return $this->render('like_it_3');
    }

    public function actionLikeIt4()
    {
        return $this->render('like_it_4');
    }

    public function actionGaleriLikeIt2021()
    {
        return $this->render('galeri_like_it_2021');
    }

    public function actionGaleriLikeIt2022()
    {
        return $this->render('galeri_like_it_2022');
    }

    public function actionGaleriLikeIt2023()
    {
        return $this->render('galeri_like_it_2023');
    }

    public function actionGaleriLikeIt2024()
    {
        return $this->render('galeri_like_it_2024');
    }
}
