<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'plugins/youtube-video-background/style.css',
        'plugins/responsive-flip-countdown/style.css',
        'fonts/gotham/gotham.css'

    ];
    public $js = [
        'https://kit.fontawesome.com/9c745d78f0.js',
        'plugins/alphanumeric-captcha/js/jquery-captcha.js',
        // 'plugins/youtube-video-background/jquery.youtube-background.min.js',
        'https://stamat.info/youtube-background/jquery.youtube-background.js',
        'https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js',
        // 'plugins/responsive-flip-countdown/jquery.flipper-responsive.js',
        'https://www.jqueryscript.net/demo/responsive-flip-countdown/jquery.flipper-responsive.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
