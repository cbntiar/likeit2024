<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost:8889;dbname=likeit2024',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => false,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
