<?php

use app\models\MContent;
use yii\helpers\Url;

$this->title = 'LIKE It #2';

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <?php if (false) : ?>
                <div class="card-body text-center" style="">
                    <h2 class="display-5">
                        <i class="fas fa-clock"></i>
                        <div>
                            Coming Soon
                        </div>
                    </h2>
                </div>
            <?php else : ?>
                <div class="card-body" style="">
                    <div class="row mb-2 d-flex justify-content-center py-4 px-sm-5">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8 mb-2">
                            <h3>Series 2 GENCARKAN Investasi Bagi Generasi Muda Menuju Indonesia Emas</h3>
                            <img src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/LIke%20it%20Update%20layout-01.jpg?updatedAt=1728031419591" alt="Flyer" width="100%" height="auto">
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 text-justify">
                            <h5>Yuk! Ikuti Literasi Keuangan Indonesia Terdepan – LIKE It #2</h5>

                            <p>Sobat LIKE It,</p>

                            <p>Menurut hasil SNLIK 2024 oleh OJK Indonesia Literasi Keuangan di Kalimantan Timur masihlah menunjukkan di bawah standar nasional yaitu 58,27%. Terdapat ketimpangan antara tingkat literasi dan inklusi keuangan di Provinsi Kalimantan Timur tahun 2024. OJK berkolaborasi dengan TPAKD dan Lembaga/Kementeterian, LIKE It Series 2 hadir dengan tema GENCARKAN Investasi Bagi Generasi Muda Menuju Indonesia Emas guna meningkatkan akses keuangan daerah dan memberikan edukasi financial planner bagi generasi muda khususnya di Kalimantan Timur.</p>

                            <p>Ikuti acaranya pada:</p>
                            <div class="text-left mb-3">
                                <div><i class="fa fa-calendar"></i> Sabtu, 5 Oktober 2024</div>
                                <div><i class="fa fa-clock"></i> Pukul 10.00 - 12.30 WITA</div>
                                <div><i class="fa fa-location-dot"></i> Pentacity Mall <br> Dan Youtube Otoritas Jasa Keuangan</div>
                            </div>

                            <p>Sampai bertemu, Sobat LIKE It!</p>

                            <p class="text-left">
                            #LIKEIT2024 #LiterasiKeuangan #EdukasiKeuangan #AksesKeuangan #FinancialPlanner
                            </p>
                        </div>

                    </div>
                </div>
            <?php endif; ?>

        </div>

    </div>
</div>