<?php

use app\models\MContent;

$this->title = "Galeri LIKE It 2021";

$content = [
    [
        'title' => 'Literasi Investasi Lintas Generasi',
        'url' => 'https://www.youtube.com/embed/HS9fmiKcYq8',
        'content' => 'LIKE It adalah program tahunan sinergi empat otoritas keuangan di Indonesia, yang telah dimulai pada tahun 2021. Seri pertama telah dibuka pada tanggal 3 Agustus 2021, mengusung tema&nbsp;<strong>&ldquo;Literasi Investasi Lintas Generasi&rdquo;</strong> memberikan pemahaman mengenai produk atau investasi di surat berharga negara yang diselenggarakan oleh Kementerian Keuangan.'
    ],
    [
        'title' => 'Yuk Berinvestasi di Pasar Modal',
        'url' => 'https://www.youtube.com/embed/Nx48CXtmjO0',
        'content' => 'Sementara Seri LIKE It 2 diselenggarakan pada tanggal 5 Agustus 2021, oleh OJK mengusung tema&nbsp;<strong>"Yuk Berinvestasi di Pasar Modal"</strong> memberikan wawasan mengenai investasi di pasar modal termasuk aspek perlindungan konsumen.'
    ],
    [
        'title' => 'Mari Bersama Membangun Negeri dengan Menjadi Investor di Negeri Sendiri',
        'url' => 'https://www.youtube.com/embed/iOVDwx7-1r0',
        'content' => 'Selanjutnya, Seri LIKE It 3 mengangkat tema&nbsp;<strong>&ldquo;Mari Bersama Membangun Negeri dengan Menjadi Investor di Negeri Sendiri&rdquo;</strong> yang diselenggarakan oleh Bank Indonesia dan turut melibatkan Lembaga Penjamin Simpanan sebagai narasumber sekaligus penutup dari rangkaian acara LIKE It tahun 2021. LIKE It 3 mengajak masyarakat dan generasi muda untuk mulai membangun negeri dengan berani untuk memulai berinvestasi.</span></p>
        <p><span style="font-size: 14pt;">Selain seri edukasi webinar, juga diselenggarakan berbagai perlombaan menarik yakni&nbsp;<em>1 Minute Video Competition</em> dan Jingle &amp; Video Clip LIKE It.'
    ],
];

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <div>
                    <?php foreach ($content as $key => $value) : ?>
                        <div class="row mb-5">
                            <div class="col-12 col-md-5">
                                <iframe style="width: 100%; height: 250px; display: table; margin-left: auto; margin-right: auto;" src="<?= $value['url'] ?>" width="483" height="271" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="heading-custom m-0 mb-3">
                                    <div><?= $value['title'] ?></div>
                                </div>
                                <div class="mb-3" style="text-align: justify;font-size: 18px;">
                                    <?= $value['content'] ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
</div>