<?php

use app\models\MContent;

$this->title = "Galeri LIKE It 2023";

$content = [
    [
        'title' => 'SERIES #1',
        'url' => 'https://www.youtube.com/embed/tVrINSIX_a0?si=e3jXI3T68JwCnskV',
        'content' => 'Coming soon'
    ],
    [
        'title' => 'SERIES #2',
        'url' => 'https://www.youtube.com/embed/C1lIGK35Rpg?si=W-TyYvno53jY1e4H',
        'content' => 'Coming soon'
    ],
    [
        'title' => 'SERIES #3',
        'url' => 'https://www.youtube.com/embed/_SUF8jo2oKQ?si=Lp6UNQSI4ZTpf49W',
        'content' => 'Coming soon'
    ],
];

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <div>
                    <?php foreach ($content as $key => $value) : ?>
                        <div class="row mb-5">
                            <div class="col-12 col-md-5">
                                <!-- <iframe style="width: 100%; height: 250px; display: table; margin-left: auto; margin-right: auto;" src="<?= $value['url'] ?>" width="483" height="271" allowfullscreen="allowfullscreen"></iframe> -->
                                 <img style="width: 100%;border: 1px solid #ffc803;border-radius: 8px;" src="https://ik.imagekit.io/d9hiweoihy/placeholder.png?updatedAt=1727134762334" alt="">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="heading-custom m-0 mb-3"><div>
                                <?= $value['title'] ?>
                                </div></div>
                                <div class="mb-3" style="text-align: justify;font-size: 18px;">
                                    <?= $value['content'] ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
</div>