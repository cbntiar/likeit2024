<?php

use app\models\MContent;

$this->title = 'Finalis CreaVid LPS 2022';

$list = [
    ['title' => 'LPS Creavid 2022 | Uangmu, Tanggung Jawabmu', 'url' => 'https://www.youtube.com/watch?v=nfWXR_eveNI'],
    // ['title' => '', 'url' => 'https://www.youtube.com/watch?v=xNiKXeXhWqs'],
    // ['title' => 'SUMRINGAH', 'url' => 'https://www.youtube.com/watch?v=qR4XMQ0uZ1E'],
    ['title' => 'Investor Ritel Bertumbuh, Indonesia Tangguh', 'url' => 'https://www.youtube.com/watch?v=VBXLfPfV_7I'],
    ['title' => 'LPS CREAVID 2022: MISS U BANK', 'url' => 'https://www.youtube.com/watch?v=SdUOsL3-8z8'],
    ['title' => '#LPSCREAVID2022 | Back to Saving for a Good Living', 'url' => 'https://www.youtube.com/watch?v=N37XD1Q6j-U'],
    ['title' => 'LPS Creavid: Memilih Bank Yang Tepat Untuk Menabung Pastikan Sudah Terdaftar LPS', 'url' => 'https://www.youtube.com/watch?v=3iQjeiqWKOw'],
    ['title' => 'PENGELOLAAN KEUANGAN YANG CERDAS UNTUK MERAIH KEBEBASAN FINANSIAL', 'url' => 'https://www.youtube.com/watch?v=4Lt8DXc3irE'],
    ['title' => 'Cara mengelola keuangan agar bisa bebas finansial | LPS Creavid Competition @LPS_IDIC', 'url' => 'https://www.youtube.com/watch?v=6HPrXMy1Euo'],
    ['title' => 'LPS CREAVID 2022: MOMED', 'url' => 'https://www.youtube.com/watch?v=SDpyhuhHPas'],
    ['title' => 'SUASANA BALI DENPASAR TERBARU 2022', 'url' => 'https://www.youtube.com/watch?v=imRUlj012BE'],
    ['title' => 'LPS Creavid Competition | Literasi keuangan Untuk Indonesia | Ricky Donald', 'url' => 'https://www.youtube.com/watch?v=co0DdQJAD1o'],
    ['title' => 'Lps Creavid: Fenomena Arisan Online Berharap Cuan Malah Berakhir Penipuan', 'url' => 'https://www.youtube.com/watch?v=7yrvmDnpZNM'],
    ['title' => 'LPS CREAVID 2022: AKU HARUS BAGAIMANA ?', 'url' => 'https://www.youtube.com/watch?v=q4xb9GrcOmc'],
    ['title' => 'CREAVID LPS 2022 : SHARING', 'url' => 'https://www.youtube.com/watch?v=pI2ijgyqRvg'],
    ['title' => 'TIPS SEDERHANA KELOLA KEUANGAN PRIBADI UNTUK MENCAPAI KEBEBASAN FINANSIAL | LPS CREAVID 2022', 'url' => 'https://www.youtube.com/watch?v=55_jRCpDX7Y'],
    ['title' => 'Literasi Keuangan Kunci Sukses UMKM di Tengah Pandemi', 'url' => 'https://www.youtube.com/watch?v=7tZ7XVzW_Gc'],
    ['title' => 'Investasi Syariah Demi Pulihnya Ekonomi Nasional Pasca Pandemi', 'url' => 'https://www.youtube.com/watch?v=QQDz8lttu6o'],
    ['title' => '#LPSCREAVID2022 : ALIT', 'url' => 'https://www.youtube.com/watch?v=-1gmQR_neY8'],
    ['title' => 'LPS CREAVID 2022 : A BETTER FUTURE', 'url' => 'https://www.youtube.com/watch?v=Ujo4rzSVUpM'],
];

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <div class="heading-custom-2" style="text-align: justify;padding: 20px !important;">
                    Yuk simak Video kreatif dan edukatif mengenai Literasi Keuangan dari ke-17 Finalis CreaVid LPS 2022!
                </div>
                <div class="row m-0 mt-3">
                    <?php foreach ($list as $key => $vid) :
                        parse_str(parse_url($vid['url'], PHP_URL_QUERY), $arr);
                        $vid_id = $arr['v'];
                    ?>
                        <div class="col-md-4 mb-3">
                            <div class="text-center h-100" style="padding: 10px;border: 2px solid #0881c8;border-radius: 10px;">
                                <a style="text-decoration: none;" href="<?= $vid['url'] ?>" target="_blank">
                                    <div class="mb-2" style="position: relative;">
                                        <!-- width="320px" height="180px" -->
                                        <img style="width: 100%;" src="https://img.youtube.com/vi/<?= $vid_id ?>/mqdefault.jpg" alt="">
                                        <i class="fas fa-play video-icon"></i>
                                    </div>
                                    <div style="color: #0881c8;font-weight: 700;">
                                        <?= $vid['title'] ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
                
            </div>
        </div>

    </div>
</div>