<?php

use app\models\MContent;

$partners = [
    ['name' => 'Otoritas Jasa Keuangan', 'logo' => Yii::getAlias('@web') . '/images/partners/OJK.png'],
    ['name' => 'Bank Indonesia', 'logo' => Yii::getAlias('@web') . '/images/partners/BI.png'],
    ['name' => 'Kementerian Keuangan', 'logo' => Yii::getAlias('@web') . '/images/partners/MOF.png'],
    ['name' => 'Lembaga Penjamin Simpanan', 'logo' => Yii::getAlias('@web') . '/images/partners/LPS.png'],
];

$data = MContent::find()->where(['type' => 'partners'])->one();

$this->title = 'Stakeholders LIKE It';
?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <!-- <div class="text-center">
                    <h1>Stakeholders</h1>
                </div> -->

                <!-- <img style="display: block; margin-left: auto; margin-right: auto;" src="https://ik.imagekit.io/d9hiweoihy/partners/partners.png?updatedAt=1690906815211" alt="" width="100%" height="auto"> -->
                <div class="d-flex" style="overflow: auto;justify-content: space-between;" >
                    <a href="https://www.ojk.go.id" target="_blank">
                        <img style="width: 100%;" src="https://ik.imagekit.io/d9hiweoihy/partners/1_lvQ-sEKB4Q?updatedAt=1690907022542" alt="">
                    </a>
                    <a href="https://www.bi.go.id" target="_blank">
                        <img style="width: 100%;" src="https://ik.imagekit.io/d9hiweoihy/partners/2_AU76XGdr-w?updatedAt=1690907048360" alt="">
                    </a>
                    <a href="https://www.kemenkeu.go.id" target="_blank">
                        <img style="width: 100%;" src="https://ik.imagekit.io/d9hiweoihy/partners/3_WNgAB5BpD?updatedAt=1690907067231" alt="">
                    </a>
                    <a href="https://www.lps.go.id" target="_blank">
                        <img style="width: 100%;" src="https://ik.imagekit.io/d9hiweoihy/partners/4_rYC8Jy5_Q?updatedAt=1690907087085" alt="">
                    </a>
                </div>

            </div>
        </div>

    </div>
</div>