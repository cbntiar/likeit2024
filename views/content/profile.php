<?php

use app\models\MContent;

$this->title = 'Profil LIKE It';

?>
<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    
    <div class="container mb-5">

        <div class="card ">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <div class="row mb-2 d-flex justify-content-center">
                    <div class="col d-xl-none">
                        <img style="margin-left: auto; margin-right: auto;" src="https://ik.imagekit.io/d9hiweoihy/likeit/logo.png?updatedAt=1690531886657" alt="Logo Like It" width="100%" height="auto">
                    </div>
                    <div class="d-none d-xl-block">
                        <img style="margin-left: auto; margin-right: auto;" src="https://ik.imagekit.io/d9hiweoihy/likeit/logo.png?updatedAt=1690531886657" alt="Logo Like It" width="400px" height="225px">
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-12 text-justify px-sm-5">
                        <p>
                            Literasi Keuangan Indonesia Terdepan (LIKE It) merupakan rangkaian kegiatan Literasi Keuangan yang diselenggarakan melalui sinergi antara BI, Kemenkeu, OJK dan LPS (yang tergabung dalam Forum Koordinasi Pembiayaan Pembangunan Melalui Pasar Keuangan (FK-PPPK). Kolaborasi penyelenggaraan LIKE It pertama kali diadakan pada tahun Agustus 2021, yang dikemas dalam rangkaian lomba dan webinar dengan hosting yang dilakukan bergantian antara lembaga. Pada 2023, program ini disepakati untuk kembali dilanjutkan, mempertimbangkan kesuksesan dan dampak positif sinergi penyelenggaraan program di 2021 dan 2022 dalam mendukung peningkatan basis investor ritel di Indonesia.
                        </p>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-12 text-justify px-sm-5">
                        <p>
                           Pada tahun 2021, LIKE It berfokus pada pengenalan investasi dan prinsip pengelolaan keuangan sebagai fondasi untuk mendorong generasi muda masuk ke pasar keuangan Indonesia. Dilanjutkan di tahun 2022, LIKE It mengusung semangat berkelanjutan daIam “Menjadi investor di negeri Sendiri” agar investasi dapat menjadi suatu habit dan life Skill, untuk bekal investasi kedepannya. LIKE It #1 yang di host oleh Bank Indonesia akan berfokus untuk memberikan pemahaman dan mempertahankan habit investasi yang telah dilakukan selama ini dan meningkatkan skill investasi yang telah terbangun. Dengan pemahaman yang komperhensif ini diharapkan tidak hanya dapat meningkatkan kuantitas tetapi juga kualitas investor ritel di Indonesia.
                        </p>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-12 text-justify px-sm-5">
                        <p>
                            Tahun 2023, LIKE It #1 yang akan dihost oleh Bank Indonesia mengusung tema “Mendorong Literasi dan Investasi Keuangan Generasi Muda Pelaku Usaha”. Pemilihan tema tersebut dilandasi semangat mendorong para peserta untuk dapat memperoleh pemahaman lebih baik dan menyeluruh tentang strategi investasi keuangan, sehingga dapat membantu mereka membangun pondasi keuangan berkelanjutan yang mendukung pengembangan kewirausahaan mereka.
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>