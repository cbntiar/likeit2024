<?php

use app\models\MContent;
use yii\helpers\Url;

$this->title = 'LIKE It #3';

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <?php if (false) : ?>
                <div class="card-body text-center" style="">
                    <h2 class="display-5">
                        <i class="fas fa-clock"></i>
                        <div>
                            Coming Soon
                        </div>
                    </h2>
                </div>
            <?php else : ?>
                <div class="card-body" style="">
                    <div class="row mb-2 d-flex justify-content-center py-4 px-sm-5">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8 mb-2">
                            <h3>Series 3 Generasi Muda Menuju Masa Depan Cerah dengan Berinvestasi di Pasar Keuangan</h3>
                            <img src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/IG%20STORY_LIKEIT%202024_ALT%203%20final.jpg?updatedAt=1730785542629" alt="Flyer" width="100%" height="auto">
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 text-justify">
                            <h5>Yuk! Ikuti Literasi Keuangan Indonesia Terdepan – LIKE It #3</h5>

                            <p>Sobat LIKE It,</p>

                            <p>Di dominasinya investor muda di Indonesia, LIKE It tahun 2024 kembali hadir dengan mengusung tema “Generasi Muda Menuju Masa Depan Cerah dengan Berinvestasi di Pasar Keuangan” untuk mendukung peningkatan basis investor ritel di Indonesia generasi muda yang berkontribusi pada stabilitas dan pertumbuhan ekonomi nasional serta secara bijak berinvestasi di pasar keuangan domestik.</p>

                            <p><i class="fa fa-store"></i> Dimeriahkan oleh booth intitusi keuangan Indonesia, seperti Livin' by Mandiri, BCA, BSI, Pegadaian, Wondr by BNI, UOB Indonesia, Panin Asset Management.</p>

                            <p>Ikuti acaranya pada:</p>
                            <div><i class="fa fa-youtube"></i> Grand Ceremony </div>

                            <div class="text-left mb-3">
                                <div><i class="fa fa-calendar"></i> Rabu, tanggal 6 November 2024</div>
                                <div><i class="fa fa-clock"></i> Pukul 10.30 – 14.15 WIB</div>
                                <div><i class="fa fa-location-dot"></i> Main Atrium Gandaria City Mall, Jakarta Selatan Dan Kanal Sosial Media Bank Indonesia</div>
                            </div>
                            <div class="text-left mb-3">
                                <div><i class="fa fa-calendar"></i> Kamis - Sabtu, tanggal 7 – 9 November 2024</div>
                                <div><i class="fa fa-clock"></i> Pukul 10.00 – 22.00 WIB </div>
                                <div><i class="fa fa-location-dot"></i> Main Atrium Gandaria City Mall, Jakarta Selatan</div>
                            </div>

                            <p>Sampai bertemu, Sobat LIKE It!</p>

                            <p class="text-left">
                            #LIKEIT2024 #LiterasiKeuangan #EdukasiKeuangan #FinancialPlanner #GenerasiMuda #BerKobar #BersamaKoordinasiMembangunPasarKeuangan
                            </p>


                            <div class="mt-5 col-12 col-sm-12 col-md-4 col-lg-12 mb-4 text-center">

                                <h5><i class="fas fa-info-circle"></i> Yuk tebak jawaban Game Password berikut </h5>

                                <a href="<?= Url::to(['/event/quiz']) ?>" class="btn btn-success btn-lg btn-block"><i class="fas fa-comment"></i> Ikuti Game Password</a>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-5">
                        <div class="col-12 col-sm-6 mb-4 text-center px-5">
                            <div class="heading-custom" style="font-size: 16px;">
                                <div>
                                    <i class="fas fa-question-circle"></i> Pertanyaan
                                </div>
                            </div>
                            <p style="font-size: 20px;">
                                Ajukan pertanyaan selama acara berlangsung
                            </p>
                            <a href="<?= Url::to(['/event/question']) ?>" target="_blank" class="btn btn-secondary btn-lg mb-3">Ajukan Pertanyaan</a>
                            <!-- <p>
                            <small class="text-muted">*link dibuka mulai 14 Agustus 2023</small>
                        </p> -->
                        </div>
                        <div class="col-12 col-sm-6 text-center px-5">
                            <div class="heading-custom" style="font-size: 16px;">
                                <div><i class="fas fa-list"></i> Post-Test & Survei</div>
                            </div>
                            <p style="font-size: 20px;">
                            Dapatkan E-Sertifikat setelah mengisi post-test dan survei berikut
                            </p>
                            <a href="https://forms.gle/nrnz7tcRwMTxT4ko9" target="_blank" class="btn btn-secondary btn-lg mb-3">Isi Survey</a>
                            <!-- <a href="<?= Url::to(['/event/coming-soon']) ?>" target="_blank" class="btn btn-secondary btn-lg mb-3">Isi Post-Test dan Survei</a> -->
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div>

    </div>
</div>