<?php

use app\models\MContent;
use yii\helpers\Url;

$this->title = 'LIKE It #1';

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <?php if (false) : ?>
                <div class="card-body text-center" style="">
                    <h2 class="display-5">
                        <i class="fas fa-clock"></i>
                        <div>
                            Coming Soon
                        </div>
                    </h2>
                </div>
            <?php else : ?>
                <div class="card-body" style="">
                    <div class="row mb-2 d-flex justify-content-center py-4 px-sm-5">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8 mb-2">
                            <h3>Series 1 SDG Talks dan Literasi Keuangan Indonesia Terdepan (LIKE It): Smart Investing for a Better Tomorrow: Youth and Sustainable Finance for the SDGs </h3>
                            <img src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Poster%20Kemenkeu%20Website%20Like%20It.jpg?updatedAt=1727355411319" alt="Flyer" width="100%" height="auto">
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 text-justify">
                            <h5>Yuk! Ikuti Literasi Keuangan Indonesia Terdepan – LIKE It #1</h5>

                            <p>Sobat LIKE It,</p>

                            <p>Kami mengundang generasi muda untuk hadir dalam acara inspiratif SDG Talks dan Literasi Keuangan Indonesia Terdepan (LIKE It), yang bertujuan meningkatkan pemahaman tentang Sustainable Development Goals (SDGs) dan literasi keuangan.</p>

                            <p>Dalam SDG Talks, berbagai ahli dari pemerintah, akademisi, mitra pembangunan, serta NGO akan berbagi wawasan mengenai pencapaian SDGs dan pentingnya kolaborasi lintas sektor. Sementara itu, LIKE It akan memperkuat literasi keuangan dengan dukungan dari empat otoritas sektor keuangan, sekaligus memperkenalkan ORI026 dan SDG Bond Ritel untuk berinvestasi sekaligus mendukung pembangunan berkelanjutan.</p>

                            <p>Ikuti acaranya pada:</p>
                            <div class="text-left mb-3">
                                <div><i class="fa fa-calendar"></i> Senin, tanggal 30 September 2024</div>
                                <div><i class="fa fa-clock"></i> Pukul 08:00 – 15:00 WIB</div>
                                <div><i class="fa fa-location-dot"></i> Auditorium Fakultas Ekonomi & Manajemen IPB University, Bogor</div>
                            </div>

                            <p>Sampai bertemu, Sobat LIKE It!</p>

                            <p class="text-left">
                                #LIKEIT2024 #LiterasiKeuangan #EdukasiKeuangan #SDGs #Investasi
                            </p>


                        </div>
                    </div>

                </div>

        </div>


    </div>
<?php endif; ?>

</div>

</div>
</div>