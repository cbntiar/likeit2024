<?php

use app\models\MContent;

$this->title = "Galeri LIKE It 2022";

$content = [
    [
        'title' => 'Sustain Habits in Inveting, Invest in Sustainable Instruments',
        'url' => 'https://www.youtube.com/embed/3WtzC9K3Aws',
        'content' => 'Seri pertama tahun 2022, mengusung tema <strong>“Sustain Habits in Inveting, Invest in Sustainable Instruments”</strong> pada opening LIKE It yang memberikan pemahaman mengenai produk atau investasi berkelanjutan negara yang diselenggarakan oleh Bank Indonesia.'
    ],
    [
        'title' => 'Investasi Hijau Investasi Masa Depan',
        'url' => 'https://www.youtube.com/embed/O2SAlFsap-I',
        'content' => 'Sementara Seri LIKE It 2 diselenggarakan pada tanggal 22 Agustus 2022, oleh Kemenkeu mengusung tema <strong>"Investasi Hijau Investasi Masa Depan"</strong> memberikan  wawasan mengenai investasi berkelanjutan terutama instrumen surat berharga.'
    ],
    [
        'title' => 'Masa Depan Cerah dengan Investasi? Siapa Takut?',
        'url' => 'https://www.youtube.com/embed/_uDC0Gpxepc',
        'content' => 'Selanjutnya, Seri LIKE It 3 mengangkat tema <strong>&ldquo;Masa Depan Cerah dengan Investasi? Siapa Takut?&rdquo;</strong> yang diselenggarakan oleh OJK. Sebagai penutup, LIKE It 3 mengajak masyarakat dan generasi muda untuk memahami berbagai instrument Keuangan berkelanjutan serta mengetahui hak konsumen dalam melakukan investasi.'
    ],
];

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <div>
                    <?php foreach ($content as $key => $value) : ?>
                        <div class="row mb-5">
                            <div class="col-12 col-md-5">
                                <iframe style="width: 100%; height: 250px; display: table; margin-left: auto; margin-right: auto;" src="<?= $value['url'] ?>" width="483" height="271" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="heading-custom m-0 mb-3"><div>
                                <?= $value['title'] ?>
                                </div></div>
                                <div class="mb-3" style="text-align: justify;font-size: 18px;">
                                    <?= $value['content'] ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
</div>