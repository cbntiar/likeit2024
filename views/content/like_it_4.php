<?php

use app\models\MContent;
use yii\helpers\Url;

$this->title = 'LIKE It #4';

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><?= $this->title ?></h1>
            </div>
            <div class="card-body text-center" style="">
                <h2 class="display-5">
                    <i class="fas fa-clock"></i>
                    <div>
                        Coming Soon
                    </div>
                </h2>
            </div>
        </div>

    </div>
</div>