<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Social Media LIKE It';

$socmed = [];

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class="">Social Media</h1>
            </div>
            <div class="card-body">
                <div class="row mb-0 mt-3" style="justify-content: center;margin: 50px 0 !important; gap: 50px;">
                    <div class="w-20 mb-3">
                        <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                            <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/instagram_2111463.png?updatedAt=1690541536246" alt="" width="40" height="40"> -->
                            <a target="_blank" href="https://www.bi.go.id">
                                <span style="font-size: 12pt;display: flex;gap: 5px;">
                                    <i class="fas fa-globe" style="font-size: 25px;"></i>
                                    <strong>Bank indonesia</strong>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="w-20 mb-3">
                        <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                            <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/instagram_2111463.png?updatedAt=1690541536246" alt="" width="40" height="40"> -->
                            <a target="_blank" href="https://www.instagram.com/bank_indonesia">
                                <span style="font-size: 12pt;">
                                    <i class="fa-brands fa-2xl fa-instagram"></i>
                                    <strong>Bank indonesia</strong>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="w-20 mb-3">
                        <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                            <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/twitter_3670151.png?updatedAt=1690541536092" alt="" width="40" height="40"> -->
                            <a target="_blank" href="https://x.com/bank_indonesia"><span style="font-size: 12pt;"><i class="fa-brands fa-2xl fa-x-twitter"></i><strong>@bank_indonesia</strong></span></a>
                        </div>
                    </div>
                    <div class="w-20 mb-3">
                        <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                            <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/youtube_1384060.png?updatedAt=1690541536280" alt="" width="40" height="40"> -->
                            <a target="_blank" href="https://www.youtube.com/channel/UCV7sYsOlkpbHybdIWt7gZbg"><span style="font-size: 12pt;"><i class="fa-brands fa-2xl fa-youtube"></i><strong>Bank Indonesia</strong></span></a>
                        </div>
                    </div>
                    <div class="w-20 mb-3">
                        <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                            <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/youtube_1384060.png?updatedAt=1690541536280" alt="" width="40" height="40"> -->
                            <a target="_blank" href="https://web.facebook.com/BankIndonesiaOfficial"><span style="font-size: 12pt;"><i class="fa-brands fa-2xl fa-facebook"></i><strong>Bank Indonesia</strong></span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>