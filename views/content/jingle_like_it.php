<?php

use app\models\MContent;

$this->title = 'Finalis Jingle LIKE It 2021';

$list = [
    ['title' => 'AYO INVESTASI / Lomba Jingle & Video Klip Literasi Keuangan Indonesia Terdepan (LIKE It)', 'url' => 'https://www.youtube.com/watch?v=PEFJ4i4RHBQ'],
    ['title' => 'LIKE It! LIKE It! | Jeremia Saputra | Jingle & Video Klip Like It', 'url' => 'https://www.youtube.com/watch?v=cmHX3woolhY'],
    ['title' => 'Jingle WE "LIKE It" - Literasi Keuangan Indonesia Terdepan', 'url' => 'https://www.youtube.com/watch?v=DYM71WshjSs'],
    ['title' => '“LIKE It" Video Klip Jingle Bank Indonesia - Dewa Krisna', 'url' => 'https://www.youtube.com/watch?v=SaCUQ9MbQ9g'],
    ['title' => 'BERSAMA LIKE It || Jingle & Video Clip Competition LIKE It', 'url' => 'https://www.youtube.com/watch?v=tlboGMTaeYo'],
    ['title' => 'LIKE It MAJU BERSAMA | JINGLE LITERASI KEUANGAN INDONESIA TERDEPAN - PITASWARA PROJECT #LIKEIT', 'url' => 'https://www.youtube.com/watch?v=IlSsmbOVWJk'],
    ['title' => 'Like it, Do it, Save it', 'url' => 'https://www.youtube.com/watch?v=x0LlJRpbAeo'],
];

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">

        <div class="card">
            <div class="card-header">
                <h1 class=""><i class="fas fa-music"></i> <?= $this->title ?></h1>
            </div>
            <div class="card-body">
                <div class="heading-custom-2" style="text-align: justify;padding: 20px !important;">
                    Yuk simak Jingle LIKE It dari 10 Finalis terpilih Jingle LIKE It 2021!
                </div>
                <div class="row m-0 mt-3">
                    <?php foreach ($list as $key => $vid) :
                        parse_str(parse_url($vid['url'], PHP_URL_QUERY), $arr);
                        $vid_id = $arr['v'];
                    ?>
                        <div class="col-md-4 mb-3">
                            <div class="text-center h-100" style="padding: 10px;border: 2px solid #0881c8;border-radius: 10px;">
                                <a style="text-decoration: none;" href="<?= $vid['url'] ?>" target="_blank">
                                    <div class="mb-2" style="position: relative;">
                                        <!-- width="320px" height="180px" -->
                                        <img style="width: 100%;" src="https://img.youtube.com/vi/<?= $vid_id ?>/mqdefault.jpg" alt="">
                                        <i class="fas fa-play video-icon"></i>
                                    </div>
                                    <div style="color: #0881c8;font-weight: 700;">
                                        <?= $vid['title'] ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
                
            </div>
        </div>

    </div>
</div>