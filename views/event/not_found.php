<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Sertifikat LIKEIT 2024';

?>

<div class="main-content">
<img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class="">Tidak ditemukan <i class="fas fa-times-circle" ></i></h1>
            </div>
            <div class="card-body text-center">
                <h2 class="display-5">
                    
                    <div>
                    <?= $message ?>
                    </div>
                </h2>
                <a href="<?= Url::to(['/site/index']) ?>" class="btn btn-success">
                <i class="fas fa-home"></i> Kembali ke halaman Utama</a>

            </div>
        </div>

    </div>
</div>