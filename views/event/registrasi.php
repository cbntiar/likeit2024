<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Registrasi Kehadiran Offline';

?>

<div class="main-content h-100" style="position: relative;">
    <img style="width: 200px;position: absolute;left: -100px;top: 60px;" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-regis" style="position: absolute;right: -22px;bottom: -31px;width: 30%;" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%202%20+%20Orang.png?updatedAt=1719634391871" alt="">

    <div class="container mb-5">
        <?php if (true) : ?>
            <div class="row" style="gap: 0px !important;">
                <div class="col-md-12">
                    <img class="mb-3 regis-title" width="100%" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/iScreen%20Shoter%20-%20Preview%20-%20240922162338.png?updatedAt=1726997041002" alt="">
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="card  form-regis  mb-5">
                        <div class="card-body row">
                            <div class="col-md-12">
                                <div style="text-align: justify;border: 3px solid #f57b1f !important;" class="p-3 border mb-3">
                                    <div class="alert alert-info mb-5">
                                        <i class="fa fa-info-circle"></i>
                                        Untuk kenyamanan dan keamanan acara, hanya peserta yang telah menerima QR Code yang dapat memasuki venue. Pastikan Anda telah menyelesaikan proses registrasi dan membawa QR Code Anda saat hari acara.
                                    </div>
                                    <?= $this->render('_form', [
                                        'model' => $model,
                                        'direct_invite' => $direct_invite
                                    ]) ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <img class="w-100 mb-5" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Flyer%20Survei%203-10-2024.png?updatedAt=1727962952084" alt="">

                </div>
            </div>
        <?php else : ?>
            <div class="row" style="gap: 0px !important;">
                <div class="col-md-12">
                    <img class="mb-3 regis-title" width="100%" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/iScreen%20Shoter%20-%20Preview%20-%20240922162338.png?updatedAt=1726997041002" alt="">
                    <div class="regis-soon">
                        <div class="card  form-regis  mb-5">
                            <div class="card-body">
                                <div class="text-center">
                                    Nantikan tanggal pendaftarannya
                                </div>
                            </div>
                        </div>
                        
                        <img class="w-100 mb-5" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Flyer%20Survei%203-10-2024.png?updatedAt=1727962952084" alt="">
                    </div>
                </div>
            </div>
           
        <?php endif; ?>

    </div>
</div>
<img class="d-none" style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&C=17&D=0&N=88923&M=0" alt="" border="0" />