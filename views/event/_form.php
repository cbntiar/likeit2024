<?php

use app\components\Helpers;
use app\models\MEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */
/** @var yii\widgets\ActiveForm $form */

// $items = ArrayHelper::map(MEvent::find()->all(), 'id', 'event_name');

$model->event_id = 1;

$kpOptions = [
    "Bank Institut Indonesia" => "Bank Institut Indonesia",
    "Departemen Audit Intern" => "Departemen Audit Intern",
    "Departemen Ekonomi dan Keuangan Syariah" => "Departemen Ekonomi dan Keuangan Syariah",
    "Departemen Hukum" => "Departemen Hukum",
    "Departemen Inovasi dan Digitalisasi Data" => "Departemen Inovasi dan Digitalisasi Data",
    "Departemen Internasional" => "Departemen Internasional",
    "Departemen Kebijakan Ekonomi dan Moneter" => "Departemen Kebijakan Ekonomi dan Moneter",
    "Departemen Kebijakan Makroprudensial" => "Departemen Kebijakan Makroprudensial",
    "Departemen Komunikasi" => "Departemen Komunikasi",
    "Departemen Kebijakan Sistem Pembayaran" => "Departemen Kebijakan Sistem Pembayaran",
    "Departemen Layanan Aset Umum dan Fasilitas" => "Departemen Layanan Aset Umum dan Fasilitas",
    "Departemen Layanan Digital dan Keamanan Siber" => "Departemen Layanan Digital dan Keamanan Siber",
    "Departemen Manajemen Risiko" => "Departemen Manajemen Risiko",
    "Departemen Manajemen Strategis dan Tata Kelola" => "Departemen Manajemen Strategis dan Tata Kelola",
    "Departemen Pengelolaan Aset Perkantoran" => "Departemen Pengelolaan Aset Perkantoran",
    "Departemen Pengelolaan Devisa" => "Departemen Pengelolaan Devisa",
    "Departemen Pengembangan dan Inovasi Digital" => "Departemen Pengembangan dan Inovasi Digital",
    "Departemen Pengelolaan dan Kepatuhan Laporan" => "Departemen Pengelolaan dan Kepatuhan Laporan",
    "Departemen Pengelolaan Moneter dan Aset Sekuritas" => "Departemen Pengelolaan Moneter dan Aset Sekuritas",
    "Departemen Pengembangan Pasar Keuangan" => "Departemen Pengembangan Pasar Keuangan",
    "Departemen Jasa Perbankan Perizinan dan Operasional Tresuri" => "Departemen Jasa Perbankan Perizinan dan Operasional Tresuri",
    "Departemen Pengelolaan Aset Perumahan dan Non Perkantoran" => "Departemen Pengelolaan Aset Perumahan dan Non Perkantoran",
    "Departemen Pengadaan Strategis" => "Departemen Pengadaan Strategis",
    "Departemen Penyelenggaraan Sistem Pembayaran" => "Departemen Penyelenggaraan Sistem Pembayaran",
    "Departemen Pengelolaan Uang" => "Departemen Pengelolaan Uang",
    "Departemen Regional" => "Departemen Regional",
    "Departemen Sumber Daya Manusia" => "Departemen Sumber Daya Manusia",
    "Departemen Surveilans Makroprudensial, Moneter dan Market" => "Departemen Surveilans Makroprudensial, Moneter dan Market",
    "Departemen Surveilans SP dan Perlindungan Konsumen" => "Departemen Surveilans SP dan Perlindungan Konsumen",
    "Departemen Statistik" => "Departemen Statistik",
    "Departemen Pengembangan Umum dan Perlindungan Konsumen" => "Departemen Pengembangan Umum dan Perlindungan Konsumen",
    "Unit Khusus Pembangunan SPU DC dan BRS" => "Unit Khusus Pembangunan SPU DC dan BRS"
];

$kpwOptions = [
    "KPw Banten" => "KPw Banten",
    "KPw Nusa Tenggara Timur" => "KPw Nusa Tenggara Timur",
    "KPw Jawa Tengah" => "KPw Jawa Tengah",
    "KPw Kalimantan Selatan" => "KPw Kalimantan Selatan",
    "KPw Solo" => "KPw Solo",
    "KPw Lhokseumawe" => "KPw Lhokseumawe",
    "KPw Kalimantan Tengah" => "KPw Kalimantan Tengah",
    "KPw Papua Barat" => "KPw Papua Barat",
    "KPw Sumatera Utara" => "KPw Sumatera Utara",
    "KPw Aceh" => "KPw Aceh",
    "KPw Tegal" => "KPw Tegal",
    "KPw Sulawesi Tenggara" => "KPw Sulawesi Tenggara",
    "KPw Sulawesi Tengah" => "KPw Sulawesi Tengah",
    "KPw Sumatera Barat" => "KPw Sumatera Barat",
    "KPw DKI Jakarta" => "KPw DKI Jakarta",
    "KPw Nusa Tenggara Barat" => "KPw Nusa Tenggara Barat",
    "KPw Sulawesi Barat" => "KPw Sulawesi Barat",
    "KPw Kalimantan Timur" => "KPw Kalimantan Timur",
    "KPw Bali" => "KPw Bali",
    "KPw Sibolga" => "KPw Sibolga",
    "KPw Kepulauan Riau" => "KPw Kepulauan Riau",
    "KPw Kalimantan Barat" => "KPw Kalimantan Barat",
    "KPw Sulawesi Selatan" => "KPw Sulawesi Selatan",
    "KPw Riau" => "KPw Riau",
    "KPw Tasikmalaya" => "KPw Tasikmalaya",
    "KPw DI Yogyakarta" => "KPw DI Yogyakarta",
    "KPw Kediri" => "KPw Kediri",
    "KPw Bengkulu" => "KPw Bengkulu",
    "KPw Cirebon" => "KPw Cirebon",
    "KPw Balikpapan" => "KPw Balikpapan",
    "KPw Malang" => "KPw Malang",
    "KPw Jambi" => "KPw Jambi",
    "KPw Purwokerto" => "KPw Purwokerto",
    "KPw Sulawesi Utara" => "KPw Sulawesi Utara",
    "KPw Kalimantan Utara" => "KPw Kalimantan Utara",
    "KPw Lampung" => "KPw Lampung",
    "KPw Jawa Barat" => "KPw Jawa Barat",
    "KPw Jember" => "KPw Jember",
    "KPw Bangka Belitung" => "KPw Bangka Belitung",
    "KPw Gorontalo" => "KPw Gorontalo",
    "KPw Jawa Timur" => "KPw Jawa Timur",
    "KPw Papua" => "KPw Papua",
    "KPw Pematangsiantar" => "KPw Pematangsiantar",
    "KPw Maluku Utara" => "KPw Maluku Utara",
    "KPw Sumatera Selatan" => "KPw Sumatera Selatan",
    "KPw Maluku" => "KPw Maluku"
];

$required = '<span style="color:red;">*</span>';
?>
<style>
    .card-radio {
        border: 2px solid #cccccc;
        cursor: pointer;
        width: 100%;
        transition: border-color 0.3s;
        margin-bottom: 0px;
    }

    .card-radio input[type="radio"] {
        display: none;
    }

    .card-radio input[type="radio"]:checked+.card {
        border-color: #f57b1f;
        background-color: #ffe294;
    }
</style>
<div class="tregistration-form">

    <?php $form = ActiveForm::begin([
        'id' => 'registrasi-form'
    ]); ?>
    <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>
    <div class="row" style="gap: 30px;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Jenis Peserta</h4>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'kategori_peserta', ['template' => Helpers::inputIcon('users', $required)])->dropDownList([
                        'Bank Indonesia' => 'Bank Indonesia',
                        'Instansi Undangan' => 'Instansi Undangan',
                        'GenBI' => 'GenBI',
                        'Umum' => 'Umum',
                    ], ['class' => 'form-control nlb', 'prompt' => 'Pilih Kategori']) ?>
                    <div class="row mb-3" id="jenis-bi" style="display: none;">
                        <div class="col-6">
                            <label class="card-radio">
                                <input type="radio" checked="true" name="tipe" value="KP">
                                <div class="card text-center p-3">
                                    <div class="card-body p-0">
                                        <h5 class="card-title mb-0">KP</h5>
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div class="col-6">
                            <label class="card-radio">
                                <input type="radio" name="tipe" value="KPW">
                                <div class="card text-center p-3">
                                    <div class="card-body p-0">
                                        <h5 class="card-title mb-0">KPW</h5>
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div id="kp-options" style="display:none;">
                        <?= $form->field($model, 'kp', ['template' => Helpers::inputIcon('', $required)])->dropDownList($kpOptions, ['prompt' => 'Pilih KP']); ?>
                    </div>
                    <div id="kpw-options" style="display:none;">
                        <?= $form->field($model, 'kpw', ['template' => Helpers::inputIcon('', $required)])->dropDownList($kpwOptions, ['prompt' => 'Pilih KPW']); ?>
                    </div>
                    <div id="keterangan-input" style="display:none;">
                        <?= $form->field($model, 'keterangan', ['template' => Helpers::inputIcon('', $required)])->textInput(['placeholder' => 'Masukkan keterangan/ instansi']); ?>
                    </div>

                </div>
            </div>


        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Data Peserta</h4>
                </div>
                <div class="panel-body">

                    <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb']) ?>

                    <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input']) ?>

                    <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'no_hp'])->label('No. Handphone (Opsional)') ?>
                    <?= $form->field($model, 'instansi', ['template' => Helpers::inputIcon('building', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb']) ?>
                    <?= $form->field($model, 'profesi', ['template' => Helpers::inputIcon('briefcase', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb']) ?>


                    <?= $form->field($model, 'kota_domisili', ['template' => Helpers::inputIcon('map', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'domisili']) ?>

                    <?= $form->field($model, 'preferensi_kehadiran', ['template' => Helpers::inputIcon('', $required)])->dropDownList([
                        'Offline' => 'Offline',
                        'Online' => 'Online',
                    ], ['class' => 'form-control nlb', 'prompt' => 'Pilih Preferensi']) ?>


                    <?= $form->field($model, 'description', ['template' => Helpers::inputIcon('', $required)])->textarea(['rows' => 6]) ?>

                    <input type="hidden" name="direct_invite" value="<?= $direct_invite ?>">
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="alert alert-info mb-5">
                <i class="fa fa-info-circle"></i>
                Ingin mendapatkan E-sertifikat acara? Yuk, lengkapi dengan mengisi Registrasi, Post-Test, dan Survei Kepuasan Penyelenggaraan Acara. Jangan lupa, sertakan bukti foto atau capture kehadiranmu—baik offline maupun online. Sertifikat menantimu setelah langkah-langkah ini selesai!
            </div>
        </div>

        <div class="col-md-12 text-center">
            <div class="form-group text-center">
                <div style="width: 200px;margin:auto;">
                    <div class="mb-3">
                        <canvas class="mb-2" id="canvas" style="border: 1px solid #6f73ca;"></canvas>
                        <input name="code" class="form-control">
                    </div>
                    <?= Html::button('Submit', ['class' => 'btn btn-main btn-lg', 'id' => 'submit-form']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>



<script>

    $('input[name="tipe"]').on('change', function() {
        $('#kp-options select').val("");
        $('#kpw-options select').val("");

        if ($(this).val() === 'KP') {
          $('#kp-options').show();
          $('#kpw-options').hide();
        } else if ($(this).val() === 'KPW') {
          $('#kp-options').hide();
          $('#kpw-options').show();
        }
    });

    $('#tregistration-kategori_peserta').change(function() {
        var value = $(this).val();
        $('#kp-options').hide();
        $('#kp-options select').val("");
        $('#kpw-options').hide();
        $('#kpw-options select').val("");
        $('#keterangan-input').hide();
        $('#keterangan-input input').val("");
        $('#jenis-bi').hide()

        if (value === 'Bank Indonesia') {
            $('#jenis-bi').show()
            $('#kp-options').show();
            // $('#kpw-options').show();
        } else if (value === 'GenBI') {
            $('#kpw-options').show();
        } else if (value === 'Instansi Undangan') {
            $('#keterangan-input').show();
        } else if (value === 'Umum') {
            $('#keterangan-input').show();
        }
    });

    const captcha = new Captcha($('#canvas'), {
        length: 4,
        resourceType: 'A',
    });

    $('#submit-form').click(function() {
        const check = captcha.valid($('input[name="code"]').val());


        const kategori_peserta = $('#tregistration-kategori_peserta').val()
        const kp = $('#tregistration-kp').val()
        const kpw = $('#tregistration-kpw').val()
        const keterangan = $('#tregistration-keterangan').val()


        const nama_lengkap = $('#tregistration-nama_lengkap').val()
        const email = $('#email-input').val()
        const profesi = $('#tregistration-profesi').val()
        const instansi = $('#tregistration-instansi').val()
        const domisili = $('#domisili').val()
        const motivasi = $('#tregistration-description').val()


        const formInput = {
            "Kategori Peserta": kategori_peserta,
            // "KP": kp,
            // "KPw": kpw,
            "Nama Lengkap": nama_lengkap,
            "Email": email,
            "Profesi": profesi,
            "Instansi": instansi,
            "Domisili": domisili,
            "Motivasi mengikuti acara": motivasi,
        }

        if (kategori_peserta === 'Bank Indonesia') {
            if (kp == '' && kpw == '') {
                alert('KP atau KPw wajib di pilih')
                return false
            }
            // if (kpw == '') {
            //     alert('KPw wajib di pilih')
            //     return false
            // }
        } else if (kategori_peserta === 'GenBI') {
            if (kpw == '') {
                alert('KPw wajib di pilih')
                return false
            }
        } else if (kategori_peserta === 'Instansi Undangan') {
            if (keterangan == '') {
                alert('Keterangan instansi undangan wajib di isi')
                return false
            }
        } else if (kategori_peserta === 'Umum') {
            if (keterangan == '') {
                alert('Keterangan umum wajib di isi')
                return false
            }
        }


        for (const key in formInput) {
            if (Object.prototype.hasOwnProperty.call(formInput, key)) {
                const _inp = formInput[key];

                if (_inp == '') {
                    alert(`${key} wajib di isi`)
                    return false
                }
            }
        }

        if (!isEmail(email)) {
            alert('Email tidak valid')
            return false
        }


        // if ($('#ktp-input').val().length != 16) {
        //     alert('No KTP harus berjumlah 16 digit')
        //     return false
        // }

        // if (!$.isNumeric($('#ktp-input').val())) {
        //     alert('No KTP tidak valid')
        //     return false
        // }

        if (check) {
            // $('#submit-form').attr('disabled', true)

            // $('#registrasi-form').submit()

            var formData = new FormData($('#registrasi-form')[0]);

            $.ajax({
                url: `<?= Url::to(['/event/send-registration']) ?>`,
                type: 'POST',
                dataType: 'JSON',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#submit-form').attr('disabled', true)
                },
                success: function(response) {
                    if (response.success) {
                        // alert('Terima kasih telah melakukan registrasi, pengumuman konfirmasi peserta akan dikirimkan melalui email terdaftar mulai tanggal 7 Agustus 2023')
                        // $('#registrasi-form')[0].reset()
                        window.location.href = '<?= Url::to(['/event/thanks']) ?>'
                    } else {
                        alert(response.message)
                    }
                },
                complete: function() {
                    $('#modal-detail').modal('hide');
                    $('#submit-form').attr('disabled', false)

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                    $('#modal-detail .modal-body').html(pesan);
                    $('#submit-form').attr('disabled', false)

                    // alert(pesan);
                }
            });
        } else {
            alert('Captcha tidak sesuai')
        }

        captcha.refresh();
    })

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    // var form = $('#registrasi-form')[0],
    //     url = 'https://script.google.com/macros/s/AKfycbwlq8T_5uX0KepWkG1sLex3FyX05vWNDO1z1o0ZuThosvSyrAggSSvgp-l4weDvu6MO/exec'

    // $(function(e) {
    //     $('#submit-form').on('click', function(e) {
    //         e.preventDefault();
    //         var jqxhr = $.ajax({
    //             url: url,
    //             method: "POST",
    //             dataType: "json",
    //             data: {
    //                 test: "test new"
    //             }
    //         }).success(
    //             // do something
    //         );
    //     })
    // });
</script>