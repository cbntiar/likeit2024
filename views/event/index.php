<?php

use app\models\TRegistration;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\models\TRegistrationSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'T Registrations';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-content">
    <div class="container mb-5">
        <h1 class="display-4">List Register</h1>

        <div class="card">
            <div class="card-body text-black">
            <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'event_id',
            'nama_lengkap',
            'no_ktp',
            'email:email',
            'profesi',
            'instansi',
            'no_hp',
            'description:ntext',
            'status',
            'created_at',
            //'updated_at',
            //'deleted_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TRegistration $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
            </div>
        </div>

    </div>
</div>
