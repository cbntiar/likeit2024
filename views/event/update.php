<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Update T Registration: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'T Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tregistration-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
