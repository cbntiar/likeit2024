<?php

use app\components\Helpers;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$this->title = 'Ajukan Pertanyaan';

$required = '<span style="color:red;">*</span>';

?>

<div class="main-content">
<img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class=""><i class="fas fa-circle-question"></i> Ajukan Pertanyaan</h1>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'question-form'
                ]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb']) ?>
                       
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input']) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'pertanyaan', ['template' => Helpers::inputIcon('', $required)])->textarea(['maxlength' => true, 'rows' => 6, 'id' => 'text-pertanyaan']) ?>
                        <div class="form-group text-center">
                            <div style="width: 300px;margin:auto;">
                                <div class="mb-3">
                                    <canvas class="mb-2" id="canvas" style="border: 1px solid #6f73ca;"></canvas>
                                    <input name="code" id="text-captcha" class="form-control">
                                </div>
                                <?= Html::button('Kirim Pertanyaan', ['class' => 'btn btn-success btn-lg', 'id' => 'submit-form']) ?>
                            </div>
                        </div>

                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
    </div>
    <img class="d-none" style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&C=17&D=0&N=88923&M=0" alt="" border="0" />

    <script>
        const captcha = new Captcha($('#canvas'), {
            length: 4,
            resourceType: 'A',
        });

        $('#submit-form').click(function(el) {
            const check = captcha.valid($('input[name="code"]').val());

            const nama_lengkap = $('#tquestion-nama_lengkap').val()
            const email = $('#email-input').val()
            const pertanyaan = $('#text-pertanyaan').val()

            const formInput = {
                "Nama Lengkap": nama_lengkap,
                "Email": email,
                "Pertanyaan": pertanyaan,
            }

            for (const key in formInput) {
                if (Object.prototype.hasOwnProperty.call(formInput, key)) {
                    const _inp = formInput[key];
                    
                    if (_inp == '') {
                        alert(`${key} wajib di isi`)
                        return false
                    }
                }
            }

            if (!isEmail($('#email-input').val())) {
                alert('Email tidak valid')
                return false
            }

            if (check) {

                var formData = new FormData($('#question-form')[0]);

                $.ajax({
                    url: `<?= Url::to(['/event/send-question']) ?>` ,
                    type: 'POST',
                    dataType: 'JSON',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('#submit-form').attr('disabled', true)
                    },
                    success: function(response) {
                        if (response.success) {
                            alert('Terima kasih, kami telah menerima pertanyaan anda')
                            $('#text-pertanyaan').val('')
                            $('#text-captcha').val('')
                        } else {
                            alert('Gagal mengirim pertanyaan, mohon hubungi administrator')
                        }
                    },
                    complete: function() {
                        $('#modal-detail').modal('hide');
                        $('#submit-form').attr('disabled', false)

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                        $('#modal-detail .modal-body').html(pesan);
                        $('#submit-form').attr('disabled', false)

                        // alert(pesan);
                    }
                });
            } else {
                alert('Captcha tidak sesuai')
            }

            captcha.refresh();
        })

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    </script>