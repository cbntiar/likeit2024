<?php

use app\components\Helpers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Konfirmasi Kehadiran';


if (!empty($model)) {
   
    $model->nama_lengkap = decrypt($model->nama_lengkap);
    $model->email = decrypt($model->email);
    $model->no_hp = decrypt($model->no_hp);
}


?>

<div class="main-content">
<img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card">
            <div class="card-body ">
                <?php if ($self == '1') : ?>
                    <div class="row mb-3">
                        <div class="col-12 col-md-8">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control nlb" placeholder="Masukkan alamat email atau nomor KTP" name="" id="email-input">
                            </div>

                        </div>
                        <div class="col-12 col-md-4">
                            <button class="btn btn-success btn-lg btn-block" id="submit-form">Submit</button>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (!empty($_GET['e'])) : ?>

                    <h2 class="display-5 text-center">
                        <?= $success ? '<i class="fas fa-check-circle"  style="color: #00ae00;font-size: 80px;margin-bottom: 20px;"></i>' : '<i class="fas fa-times-circle" style="color: #ff3a3a;font-size: 80px;margin-bottom: 20px;"></i>' ?>
                        <div>
                            <?= $message ?>
                        </div>
                    </h2>
                <?php endif; ?>

                <?php if (!empty($model)) : ?>
                    <div>
                        <?php $form = ActiveForm::begin([
                            'id' => 'registrasi-form'
                        ]); ?>
                        <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                                <!-- <?= $form->field($model, 'no_ktp', ['template' => Helpers::inputIcon('id-card')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'ktp-input', 'disabled' => true]) ?> -->
                                <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input', 'disabled' => true]) ?>
                                <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'no_hp', 'disabled' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'profesi', ['template' => Helpers::inputIcon('briefcase')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                                <?= $form->field($model, 'instansi', ['template' => Helpers::inputIcon('building')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                                
                            </div>
                            <div class="col-md-12 text-center">
                                <?= $form->field($model, 'description')->textarea(['rows' => 6, 'disabled' => true]) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php endif; ?>



            </div>
        </div>

    </div>
</div>

<script>
    $('#submit-form').click(function() {
        var email = $('#email-input').val()
        // if (!isEmail(email)) {
        //     alert('Email tidak valid')
        //     return false
        // }
        window.location.href = '<?= Url::to(['/event/konfirmasi', 'self' => '1']) ?>&e='+email
    })

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>