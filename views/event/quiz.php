<?php

use app\components\Helpers;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$this->title = 'Quiz LIKE It 2023';

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class=""><i class="fas fa-circle-quiz"></i> Submit Jawaban</h1>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'quiz-form'
                ]); ?>

                <div class="row">
                    <div class="col-md-12">
                        <div style="text-align: justify;border: 2px solid #0a4f92 !important;border-radius: 5px;" class="p-3 border mb-3">
                            <p class="text-center" style="font-size: 20px;font-weight: 600;">Term & Condition</p>
                            <p>
                                *Pemenang yang menjawab benar dan tercepat setelah 3 clue diberikan adalah pemenangnya.
                            </p>

                            <p>
                                *Peserta hanya diperbolehkan untuk menjawab satu kali jawaban. Jika peserta menjawab jawaban lebih satu kali maka akan di sort dan dipilih jawaban tercepat yang di submit.
                            </p>

                            <p>
                                *Cut off penutupan jawaban adalah selesainya acara LIKE It yakni pukul 14.15 WIB. Maka Form submit jawaban akan ditutup pukul 14.15 WIB.
                            </p>

                            <p>
                                *Pemenang Utama akan dicantumkan dalam website LIKE It, dan dan akan dihubungi oleh panitia.
                            </p>

                            <p>
                                *Keputusan juri tidak dapat diganggu gugat.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'nama-lengkap']) ?>
                        <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'no-hp'])->label('No. Handphone (Opsional)') ?>
                        <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input']) ?>
                    </div>
                    <div class="col-md-7">
                        <?= $form->field($model, 'jawaban')->textarea(['maxlength' => true, 'rows' => 6, 'id' => 'text-jawaban']) ?>
                        <div class="form-group text-center">
                            <div style="width: 300px;margin:auto;">
                                <div class="mb-3">
                                    <canvas class="mb-2" id="canvas" style="border: 1px solid #6f73ca;"></canvas>
                                    <input name="code" id="text-captcha" class="form-control">
                                </div>
                                <?= Html::button('Kirim Jawaban', ['class' => 'btn btn-success btn-lg', 'id' => 'submit-form']) ?>
                            </div>
                        </div>

                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
    </div>
    <img class="d-none" style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&C=17&D=0&N=88923&M=0" alt="" border="0" />
    <script>
        const captcha = new Captcha($('#canvas'), {
            length: 4,
            resourceType: 'A',
        });

        function convertTZ(date, tzString) {
            return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {
                timeZone: tzString
            }));
        }

        $('#submit-form').click(function(el) {
            const check = captcha.valid($('input[name="code"]').val());

            if (!isEmail($('#email-input').val())) {
                alert('Email tidak valid')
                return false
            }

            // if (check) {

            //     var date = new Date(Date.now()) 
            //     date = convertTZ(date, "Asia/Jakarta")
            //     var tanggal = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`

            //     var formData = new FormData();

            //     formData.append('nama_lengkap', $('#nama-lengkap').val())
            //     formData.append('email', $('#email-input').val())
            //     formData.append('no_hp', $('#no-hp').val())
            //     formData.append('jawaban', $('#text-jawaban').val())
            //     formData.append('tanggal', tanggal)


            //     $.ajax({
            //         url: `https://script.google.com/macros/s/AKfycbwdw55aQVKo3y_HJ4qkuYJP-brLVZYF5KrPxRICQz-1-XC6lTkfahy6MpybsWrOqqzWbw/exec?sheet_name=Quiz` ,
            //         type: 'POST',
            //         dataType: 'JSON',
            //         data: formData,
            //         processData: false,
            //         contentType: false,
            //         beforeSend: function() {
            //             $('#submit-form').attr('disabled', true)
            //         },
            //         success: function(response) {
            //             if (response.result == 'success') {
            //                 alert('Terima kasih, kami telah menerima jawaban anda')
            //                 $('#text-jawaban').val('')
            //                 $('#text-captcha').val('')
            //             } else {
            //                 alert('Gagal mengirim jawaban, mohon hubungi administrator')
            //             }
            //         },
            //         complete: function() {
            //             $('#submit-form').attr('disabled', false)

            //         },
            //         error: function(xhr, ajaxOptions, thrownError) {
            //             var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
            //             $('#submit-form').attr('disabled', false)

            //             // alert(pesan);
            //         }
            //     });
            // } else {
            //     alert('Captcha tidak sesuai')
            // }
            if (check) {

                var formData = new FormData($('#quiz-form')[0]);

                $.ajax({
                    url: `<?= Url::to(['/event/send-quiz']) ?>`,
                    type: 'POST',
                    dataType: 'JSON',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('#submit-form').attr('disabled', true)
                    },
                    success: function(response) {
                        if (response.success) {
                            alert('Terima kasih, kami telah menerima jawaban anda')
                            $('#text-pertanyaan').val('')
                            $('#text-captcha').val('')
                        } else {
                            alert('Gagal mengirim jawaban, mohon hubungi administrator')
                        }
                    },
                    complete: function() {
                        $('#modal-detail').modal('hide');
                        $('#submit-form').attr('disabled', false)

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                        $('#modal-detail .modal-body').html(pesan);
                        $('#submit-form').attr('disabled', false)

                        // alert(pesan);
                    }
                });
            } else {
                alert('Captcha tidak sesuai')
            }

            captcha.refresh();

            captcha.refresh();
        })

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    </script>