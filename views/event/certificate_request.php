<?php

use app\components\Helpers;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Request Sertifikat';

$model->event_id = 1;

$required = '<span style="color:red;">*</span>';

?>

<div class="main-content">
    <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class=""><i class="fas fa-certificate"></i> Request Sertifikat</h1>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'cert-form'
                ]); ?>
                <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>

                <div class="row">
                    <div class="col-md-12">
                    <div class="alert alert-success mb-5">
                        <i class="fa fa-info-circle"></i>
                        Formulir request sertifikat Diperpanjangan hingga <span>Jumat, 22 November 2024</span>
                    </div>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user', $required)])->textInput(['maxlength' => '25', 'class' => 'form-control nlb', 'placeholder' => 'Maksimal 25 karakter']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb'])->label('No. Handphone (Opsional)') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'instansi', ['template' => Helpers::inputIcon('building', $required)])->textInput(['maxlength' => '35', 'placeholder' => 'Maksimal 35 karakter', 'class' => 'form-control nlb', 'id' => 'instansi-input']) ?>
                    </div>
                    <div class="col-md-6">
                        <!-- <?= $form->field($model, 'image', ['template' => Helpers::inputIcon('image', $required)])->textInput(['maxlength' => true, 'class' => 'form-control nlb']) ?> -->
                        <div class="form-group field-tcertificaterequest-image">

                            <label class="control-label" for="tcertificaterequest-image">Foto Bukti Kehadiran <?= $required ?></label> <span class="text-muted small ml-2" style="font-style: italic;">(Maksimal 2Mb dengan format JPG/JPEG/PNG)<span>
                                    <div class="input-group mb-1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-image" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="file" id="tcertificaterequest-image" class="form-control nlb" name="TCertificateRequest[image]">
                                    </div>
                                    <span class="text-muted small" style="font-style: italic;">Foto atau Capture menghadiri event LIKE It secara offline ataupun online<span>

                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <div class="form-group text-center">
                            <div style="width: 200px;margin:auto;">
                                <div class="mb-3">
                                    <canvas class="mb-2" id="canvas" style="border: 1px solid #6f73ca;"></canvas>
                                    <input name="code" class="form-control">
                                </div>
                                <?= Html::button('Request Sertifikat', ['class' => 'btn btn-success btn-lg', 'id' => 'submit-form']) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
<img class="d-none" style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&C=17&D=0&N=88923&M=0" alt="" border="0" />

<script>
    const captcha = new Captcha($('#canvas'), {
        length: 4,
        resourceType: 'A',
    });

    $('#submit-form').click(function() {

        const check = captcha.valid($('input[name="code"]').val());

        const nama_lengkap = $('#tcertificaterequest-nama_lengkap').val()
        const email = $('#email-input').val()
        const image = $('#tcertificaterequest-image').val()

        const formInput = {
            "Nama Lengkap": nama_lengkap,
            "Email": email,
            "Foto Bukti Kehadiran": image,
        }

        for (const key in formInput) {
            if (Object.prototype.hasOwnProperty.call(formInput, key)) {
                const _inp = formInput[key];

                if (_inp == '') {
                    alert(`${key} wajib di isi`)
                    return false
                }
            }
        }

        if (!isEmail($('#email-input').val())) {
            alert('Email tidak valid')
            return false
        }

        if (check) {
            var formData = new FormData($('#cert-form')[0]);

            // $.ajax({
            //     url: `<?= Url::to(['/event/send-certificate-request']) ?>`,
            //     type: 'POST',
            //     dataType: 'JSON',
            //     data: formData,
            //     processData: false,
            //     contentType: false,
            //     beforeSend: function() {
            //         $('#submit-form').attr('disabled', true)
            //     },
            //     success: function(response) {
            //         if (response.success) {
            //             // alert('Terima kasih, kami telah menerima permintaan anda')
            //             Swal.fire({
            //                 title: "Terima kasih",
            //                 text: "kami telah menerima permintaan anda",
            //                 icon: "success",
            //                 showCancelButton: false,
            //                 confirmButtonColor: "#3085d6",
            //                 cancelButtonColor: "#d33",
            //                 confirmButtonText: "Ok"
            //                 }).then((result) => {
            //                     if (result.isConfirmed) {
            //                         window.open('https://forms.gle/nrnz7tcRwMTxT4ko9', '_blank')
            //                     }
            //                 });
            //             // $('#text-captcha').val('')
            //             $('#cert-form')[0].reset()
            //         } else {
            //             alert(response.message)
            //         }
            //     },
            //     complete: function() {
            //         $('#modal-detail').modal('hide');
            //         $('#submit-form').attr('disabled', false)

            //     },
            //     error: function(xhr, ajaxOptions, thrownError) {
            //         var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
            //         $('#modal-detail .modal-body').html(pesan);
            //         $('#submit-form').attr('disabled', false)

            //         // alert(pesan);
            //     }
            // });

            Swal.fire({
                title: 'Kirim Permintaan?',
                text: "Apakah Anda yakin ingin mengirim permintaan sertifikat?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, kirim!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Menampilkan loader sebelum request AJAX dimulai
                    Swal.fire({
                        title: 'Mengirim...',
                        text: 'Silakan tunggu beberapa saat.',
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });

                    $.ajax({
                        url: `<?= Url::to(['/event/send-certificate-request']) ?>`,
                        type: 'POST',
                        dataType: 'JSON',
                        data: formData,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#submit-form').attr('disabled', true);
                        },
                        success: function(response) {
                            if (response.success) {
                                Swal.fire({
                                    title: "Terima kasih",
                                    text: "Kami telah menerima permintaan Anda",
                                    icon: "success",
                                    confirmButtonColor: "#3085d6",
                                    confirmButtonText: "Ok"
                                }).then((result) => {
                                    // if (result.isConfirmed) {
                                    //     window.open('https://forms.gle/nrnz7tcRwMTxT4ko9', '_blank');
                                    // }

                                    window.location.href = '<?= Url::to(['/site/index']) ?>'
                                });
                                $('#cert-form')[0].reset();
                            } else {
                                Swal.fire({
                                    title: "Gagal",
                                    text: response.message,
                                    icon: "error",
                                    confirmButtonColor: "#3085d6",
                                    confirmButtonText: "Ok"
                                });
                            }
                        },
                        complete: function() {
                            $('#modal-detail').modal('hide');
                            $('#submit-form').attr('disabled', false);
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                            $('#modal-detail .modal-body').html(pesan);
                            $('#submit-form').attr('disabled', false);
                            Swal.fire({
                                title: "Error",
                                text: pesan,
                                icon: "error",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "Ok"
                            });
                        }
                    });
                }
            });


        } else {
            alert('Captcha tidak sesuai')
        }

        captcha.refresh();
    })

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>