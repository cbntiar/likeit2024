<?php

use app\components\Helpers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Konfirmasi Kehadiran';

if (!empty($model)) {
   
    $model->nama_lengkap = decrypt($model->nama_lengkap);
    $model->email = decrypt($model->email);
    $model->no_hp = decrypt($model->no_hp);
}


?>

<div>
<audio id="alertSoundError" src="<?= Yii::getAlias('@web') ?>/error.mp3" preload="auto"></audio>
<audio id="alertSoundSuccess" src="<?= Yii::getAlias('@web') ?>/success.mp3" preload="auto"></audio>

<?php if (!empty($model)): ?>
    <h2 class="display-5 text-center">
        <?= $success ? '<i class="fas fa-check-circle"  style="color: #00ae00;font-size: 80px;margin-bottom: 20px;"></i>' : '<i class="fas fa-times-circle" style="color: #ff3a3a;font-size: 120px;margin-bottom: 20px;"></i>' ?>
        <div>
            <?= $message ?>
        </div>
    </h2>

   

    <?php $form = ActiveForm::begin([
        'id' => 'registrasi-form'
    ]); ?>
    <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
            <!-- <?= $form->field($model, 'no_ktp', ['template' => Helpers::inputIcon('id-card')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'ktp-input', 'disabled' => true]) ?> -->
            <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input', 'disabled' => true]) ?>
            <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'no_hp', 'disabled' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'profesi', ['template' => Helpers::inputIcon('briefcase')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
            <?= $form->field($model, 'instansi', ['template' => Helpers::inputIcon('building')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
        </div>
        <div class="col-md-12 text-center">
            <?= $form->field($model, 'description')->textarea(['rows' => 6, 'disabled' => true]) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
<?php else: ?>
    <h2 class="display-5 text-center">
            <i class="fas fa-times-circle"  style="color: #ff3a3a;font-size: 80px;margin-bottom: 20px;"></i>
            <div>
                Data peserta tidak ditemukan.
            </div>
        </h2>
<?php endif; ?>
<?php if ($success) {
    echo '<script>$("#alertSoundSuccess")[0].play();</script>';
} else {
    echo '<script>$("#alertSoundError")[0].play();</script>';
} ?>
</div>
