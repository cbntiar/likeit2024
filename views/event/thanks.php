<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Registrasi Event';

?>

<div class="main-content">
<img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class="">Registrasi Berhasil <i class="fas fa-check-circle" ></i></h1>
            </div>
            <div class="card-body text-center">
                <h2 class="display-5">
                    
                    <div>
                    Terima kasih telah melakukan registrasi, pengumuman konfirmasi peserta akan dikirimkan melalui email terdaftar mulai tanggal 4 November 2024
                    </div>
                </h2>
                <a href="<?= Url::to(['/site/index']) ?>" class="btn btn-success">
                <i class="fas fa-home"></i> Kembali ke halaman Utama</a>

            </div>
        </div>

    </div>
</div>