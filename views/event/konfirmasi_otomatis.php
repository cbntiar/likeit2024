<?php

use app\components\Helpers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Konfirmasi Kehadiran';

?>

<div class="main-content">
<img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201%20(1).png?updatedAt=1727364604511" alt="">
    <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
    <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
    <div class="container mb-5">
        <div class="card">
            <div class="card-body ">
            <h2 class="display-5 text-center">
                Silakan pindai kode QR untuk melakukan konfirmasi kehadiran Anda.
                </h2>

                <div class="row mb-3">
                    <div class="col-12 col-md-12">
                    <!-- <input style="" type="text" class="" name="" id="email-input"> -->
                    <input style="opacity: 0;height: 5px;" type="text" class="" name="" id="email-input">

                    </div>
                </div>

               
                
                <div id="result-confirm"></div>


            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        $('#navbarNavDropdown').remove()
        $('footer').remove()
    })
    $('#email-input').focus()

    document.addEventListener('click', (event) => {
        const input = document.getElementById('email-input');
        if (document.activeElement !== input) {
            input.focus();
        }
    });

    // $('#submit-form').click(function() {
    //     var email = $('#email-input').val()
    //     // if (!isEmail(email)) {
    //     //     alert('Email tidak valid')
    //     //     return false
    //     // }
    //     window.location.href = '<?= Url::to(['/event/konfirmasi', 'self' => '1']) ?>&e='+email
    // })

    const extractEmail = (url) => {
        // const emailRegex = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/;
        // const emailMatch = url.match(emailRegex);
        // return emailMatch ? emailMatch[0] : null;

        const encrypted_url = new URL(url);

        // Mengambil parameter `e` dari URL
        const eParam = encrypted_url.searchParams.get("e");

        return eParam
    };


    $('#email-input').on('keyup', async function(e) {

        if (e.key === 'Enter' || e.keyCode === 13) {
            let email = extractEmail($(e.target).val())

            if (email != null) {
                // alert('email valid')

                $.ajax({
                    url: `<?= Url::to(['/event/konfirmasi-otomatis']) ?>` ,
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        email
                    },
                    beforeSend: function() {
                        $(e.target).val('')
                        $(e.target).focus()
                        $('#result-confirm').html('<div class="text-center">Memverifikasi data peserta...</div>')
                        // $('#submit-form').attr('disabled', true)
                    },
                    success: function(response) {
                        $('#info-confirm').hide()
                        $('#result-confirm').html(response)
                    },
                    complete: function() {
                        // $('#modal-detail').modal('hide');
                        // $('#submit-form').attr('disabled', false)
                        

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                        // $('#modal-detail .modal-body').html(pesan);
                        // $('#submit-form').attr('disabled', false)

                        // alert(pesan);
                    }
                });
                
            }
        }
        
    })

    

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>