<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Create T Registration';
$this->params['breadcrumbs'][] = ['label' => 'T Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tregistration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
