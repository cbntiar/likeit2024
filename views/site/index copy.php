<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'LIKEIT';
?>

<div class="site-index">

    <div class="body-content">
        <!-- <div id="carouselExampleIndicators" class="carousel slide mb-5" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php foreach ($carousel as $key => $value) : ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $key ?>" class="active"></li>
                <?php endforeach; ?>
            </ol>
            <div class="carousel-inner">
                <?php foreach ($carousel as $key => $value) : ?>
                    <div class="carousel-item <?= $key == 0 ? 'active' : '' ?>">
                        <img class="d-block w-100" src="<?= $value->image ?>" alt="First slide">
                    </div>
                <?php endforeach; ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> -->

        <div class="">
            <div class="row">
                <div class="col-md-12">
                <div class="example-marquee">
          <div id="vbg" data-vbg-mobile="true" data-vbg-load-background="true" data-vbg="https://www.youtube.com/watch?v=tVrINSIX_a0&ab_channel=BankIndonesia"></div>
          <div class="content">
            <div class="inner" style="background: #00000075;">
              <!-- <h1>Cake in a frying pan? Impossible!</h1> -->
              <div>
              <img class="navbar-logo" width="400px" src="<?= Yii::getAlias('@web') ?>/images/navbar_logo.png" alt="">
              </div>
              <div style="font-size: 36px;color: white;">
                10, 11, 12, 13, 14 JULI 2024
              </div>
              <div style="font-size: 16px;color: white;">
                MALL KOTA KASABLANKA, JAKARTA SELATAN
              </div>
              <div>
                REGISTRASI DISINI
              </div>
              <!-- <button class="vbg-destroy" data-selector="#vbg">Destroy</button>
              <button class="vbg-initialize" data-selector="#vbg" data-src="https://www.youtube.com/watch?v=LC5rEhxGqT4">Initialize</button> -->

              <!-- <div data-target="#vbg" class="seek-bar-wrapper" style="width: calc(100% - 32px); position: absolute; bottom: 16px; left: 16px;">
                <progress class="seek-bar-progress js-seek-bar-progress" value="0" max="100" aria-hidden="true"></progress>
                <input type="range" value="0" min="0" max="100" step="any" aria-label="Seek" class="seek-bar js-seek-bar">
              </div> -->
            </div>
          </div>
        </div>
                </div>
            </div>

        </div>


        <div class="container">
            <div class="card mb-5">
                <div class="card-header">
                    <h1 class="">LIKE IT 2023</h1>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-12">
                            <img class="w-100 mb-4" src="https://ik.imagekit.io/d9hiweoihy/banner/new@2x.png?updatedAt=1691566336278" alt="">

                            <img class="w-100" src="https://ik.imagekit.io/d9hiweoihy/banner/Poster2_Regist%20Feed.png?updatedAt=1693996657884" alt="">

                            <div class="row action-button my-3" style="margin: 0px;">
                                <div class="col-md-8 col-sm-12" style="">
                                    <h5><i class="fas fa-info-circle"></i> Yuk Ikutan Literasi Keuangan Indonesia Terdepan - LIKE IT #3</h5>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <a href="<?= Url::to(['/content/like-it-3']) ?>" class="btn btn-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Selengkapnya</a>
                                </div>
                            </div>

                            <img class="w-100 mb-4" src="https://ik.imagekit.io/d9hiweoihy/banner/Flyer%20copy%202@2x.png?updatedAt=1693060249421" alt="">

                            <!-- <div class="row action-button my-3" style="margin: 0px;">
                                <div class="col-md-8 col-sm-12" style="">
                                    <h5><i class="fas fa-info-circle"></i> Yuk Ikutan Literasi Keuangan Indonesia Terdepan - LIKE IT #2</h5>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <a href="<?= Url::to(['/content/like-it-2']) ?>" class="btn btn-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Selengkapnya</a>
                                </div>
                            </div> -->



                            <img class="w-100" src="https://ik.imagekit.io/d9hiweoihy/banner/IG%20POST_LIKEIT%202023%20OK-01.jpg?updatedAt=1691500442139" alt="">
                            <!-- <div class="row action-button my-3" style="margin: 0px;">
                                <div class="col-md-8 col-sm-12" style="">
                                    <h5><i class="fas fa-info-circle"></i> Registrasi dibuka pada tanggal 4 Agustus 2023 - 11 Agustus 2023</h5>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <a href="<?= Url::to(['/event/registrasi']) ?>" class="btn btn-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Klik untuk mendaftar</a>
                                </div>
                            </div> -->
                            <hr>
                        </div>
                        <div class="col-md-12 mb-5">
                            <!-- <div class="card mb-3"> -->
                            <!-- <div class="card-body pb-0"> -->
                            <div class="heading-custom"><i class="fa-brands fa-youtube"></i> Live Streaming LIKE IT 2023</div>
                            <div class="mb-3">
                                <div style="font-size: 24px;font-weight: 500;">Saksikan keseruan LIKE IT 2023, 14 Agustus 2023, Live pukul 08.30-12.30 WIB</div>
                                <!-- <div style="    color: #7e7e7e;">Saksikan keseruan LIKE IT 2023, 14 Agustus 2023, Live pukul 08.30-12.30 WIB</div> -->
                            </div>
                            <iframe width="100%" height="500" src="https://www.youtube.com/embed/tVrINSIX_a0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-9 mb-3">
                            <!-- <div class="card"> -->
                            <!-- <div class="card-body pb-0"> -->
                            <div class="heading-custom">Social Media LIKE IT</div>
                            <div class="row mb-0 mt-3" style="justify-content: space-between;">
                                <div class="col-xs-6 col-sm-6 col-lg-3 mb-3 p-0">
                                    <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                        <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/instagram_2111463.png?updatedAt=1690541536246" alt="" width="40" height="40"> -->
                                        <a target="_blank" href="https://www.bi.go.id">
                                            <span style="font-size: 12pt;display: flex;gap: 5px;">
                                                <i class="fas fa-globe" style="font-size: 25px;"></i>
                                                <strong>Bank indonesia</strong>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-lg-3 mb-3 p-0">
                                    <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                        <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/instagram_2111463.png?updatedAt=1690541536246" alt="" width="40" height="40"> -->
                                        <a target="_blank" href="https://www.instagram.com/bank_indonesia">
                                            <span style="font-size: 12pt;">
                                                <i class="fa-brands fa-2xl fa-instagram"></i>
                                                <strong>Bank indonesia</strong>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-lg-3 mb-3 p-0">
                                    <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                        <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/twitter_3670151.png?updatedAt=1690541536092" alt="" width="40" height="40"> -->
                                        <a target="_blank" href="https://twitter.com/bank_indonesia"><span style="font-size: 12pt;"><i class="fa-brands fa-2xl fa-twitter"></i><strong>@bank_indonesia</strong></span></a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-lg-3 mb-3 p-0">
                                    <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                        <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/youtube_1384060.png?updatedAt=1690541536280" alt="" width="40" height="40"> -->
                                        <a target="_blank" href="https://www.youtube.com/channel/UCV7sYsOlkpbHybdIWt7gZbg"><span style="font-size: 12pt;"><i class="fa-brands fa-2xl fa-youtube"></i><strong>Bank Indonesia</strong></span></a>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->
                            <!-- </div> -->
                        </div>
                        <div class="col-md-3 mb-3">
                            <!-- <div class="card"> -->
                            <!-- <div class="card-body pb-0"> -->
                            <div class="heading-custom d-flex justify-content-between" style="    height: 100px;    align-items: center;padding: 20px 20px;">
                                <div style="font-size: 20px;">
                                    Visitor
                                </div>
                                <div id="visitor" style="font-size: 24px;color: #dd4c2a;">

                                </div>
                            </div>
                            <!-- <div class="mb-4">
                                
                            </div> -->
                            <!-- </div> -->
                            <!-- </div> -->
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<script>
    $('#visitor').html('<img style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&C=17&D=0&N=88923&M=0" alt="" border="0" />')

    jQuery('[data-vbg]').youtube_background();
</script>