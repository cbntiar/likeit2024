<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'LIKEIT';
?>
<!-- OwlCarousel CSS CDN -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.css"> -->

<!-- OwlCarousel JS CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.js"></script> -->
<style>
    .owl-carousel .item {
        /* height: 10rem;
            background: #4DC7A0; */
        padding: 1rem;
        margin: 1rem;
        /* color: #FFF; */
        /* border-radius: 3px; */
        /* text-align: center; */
    }

    .owl-nav {
        position: absolute;
        top: 50%;
        width: 100%;
        display: flex;
        justify-content: space-between;
        transform: translateY(-50%);
    }

    .owl-nav .owl-prev,
    .owl-nav .owl-next {
        background: #fff;
        border-radius: 50%;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
        width: 40px;
        height: 40px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .owl-nav .owl-prev {
        left: -20px;
    }

    .owl-nav .owl-next {
        right: -20px;
    }

    .detail-arrow {
        position: absolute;
        top: calc(100vh - 200px);
        /* left: calc(50% - 130px); */
        left: 0;
        right: 0;
        /* width: 0; */
        /* height: 30px; */
        /* border: 2px solid; */
        /* border-radius: 2px; */
        animation: jumpInfinite 1.5s infinite;
    }

    .down-arrow:after {
        content: " ";
        position: absolute;
        top: 12px;
        left: -10px;
        width: 16px;
        height: 16px;
        border-bottom: 4px solid;
        border-right: 4px solid;
        border-radius: 4px;
        transform: rotateZ(45deg);
    }

    @keyframes jumpInfinite {
        0% {
            margin-top: 0;
        }

        50% {
            margin-top: 20px;
        }

        100% {
            margin-top: 0;
        }
    }

    .fade-out {
        opacity: 1;
        visibility: visible;
        transition: opacity 0.5s ease-out, visibility 0.5s ease-out;
    }

    .fade-out.hidden {
        opacity: 0;
        visibility: hidden;
        /* Agar elemen tidak bisa diakses lagi setelah fade out */
    }
</style>

<?php

$event = [
    [
        "title" => "LIKE It 2021",
        "url" => "https://www.youtube.com/embed/HS9fmiKcYq8",
        "description" => 'LIKE It adalah program tahunan sinergi empat otoritas keuangan di Indonesia, yang telah dimulai pada tahun 2021. Seri pertama telah dibuka pada tanggal 3 Agustus 2021, mengusung tema “Literasi Investasi Lintas Generasi”, "Yuk Berinvestasi di Pasar Modal", dan “Mari Bersama Membangun Negeri dengan Menjadi Investor di Negeri Sendiri”.',
    ],
    [
        "title" => "LIKE It 2022",
        "url" => "https://www.youtube.com/embed/3WtzC9K3Aws",
        "description" => 'LIKE IT kembali hadir dengan semangat baru, mengusung tema "Sustain Habits in Investing, Invest in Sustainable Instruments," "Investasi Hijau, Investasi Masa Depan," dan "Masa Depan Cerah dengan Investasi? Siapa Takut?" yang bertujuan mendorong generasi muda Indonesia dapat menjadi kebiasaan dan life skill yang berkelanjutan.',
    ],
    [
        "title" => "LIKE It 2023",
        "url" => "https://www.youtube.com/embed/tVrINSIX_a0",
        "description" => 'Peran generasi muda dalam UMKM dan investasi ritel semakin besar loh. Dalam upaya meningkatkan pemahaman tentang dukungan wirausaha, pilihan investasi, dan pengelolaan keuangan bagi generasi muda, Kementerian Keuangan bersama BI, OJK dan LPS yang tergabung dalam Forum Koordinasi Pembiayaan Pembangunan Melalui Pasar Keuangan (FK-PPPK).',
    ],
];

$agenda = [
    [
        "title" => "LIKE It #1",
        "time" => "30 September 2024 | 08.00 – 15.05",
        "place" => "Kampus IPB, Dramaga-Bogor",
        "topic" => "SDG Talks dan Literasi Keuangan Indonesia Terdepan (LIKE It): Smart Investing for a Better Tomorrow: Youth and Sustainable Finance for the SDGs",
        "list" => [
            "Welcoming Speech Rektor Institut Pertanian Bogor",
            "Speech dari Kementerian Keuangan",
            "Pembuka Masa Penawaran ORI026 (SDG Bond Ritel)",
            "Talkshow 1: SDGs Talks",
            "Talkshow 2: Literasi Keuangan",
            "Booth Literasi Produk/Kebijakan Lembaga",
        ],
        "color1" => "#df0d90",
        "color2" => "#ffdcf2",
    ],
    [
        "title" => "LIKE It #2",
        "time" => "5 Oktober 2024 | 10.00 - 12.30 WITA",
        "place" => "Pentacity Mall dan Youtube Otoritas Jasa Keuangan",
        "topic" => "GENCARKAN Investasi Bagi Generasi Muda Menuju Indonesia Emas",
        "list" => [
            "Laporan Kepala Otoritas Jasa Keuangan Provinsi Kalimantan Timur dan Kalimantan Utara",
            "Keynote Speech oleh Kepala Eksekutif Pengawas Perilaku Pelaku Usaha Jasa, Edukasi, dan Pelindungan Konsumen (KE PEPK)",
            "Diskusi Panel Sesi 1 Talk Show Edukasi Keuangan",
            "Diskusi Panel Sesi 2 Talk Show Edukasi Keuangan ",
            "Pre Test dan Post Test",
            "Art & Music Performance",
            "Booth Literasi Produk/Kebijakan Lembaga",
            "Live melalui Youtube OJK Indonesia",
        ],
        "color1" => "#ec702a",
        "color2" => "#ffdbc6",
    ],
    [
        "title" => "LIKE It #3",
        "time" => "6 November 2024 | 10.00 – 14.15",
        "place" => "Main Atrium Gandaria City, Jakarta Selatan",
        "topic" => "Generasi Muda Menuju Masa Depan Cerah dengan Berinvestasi di Pasar Keuangan",
        "list" => [
            "Leaders’ Insight: Gubernur BI, Menteri Keuangan, Ketua Dewan Komisioner OJK, Ketua Dewan Komisioner LPS",
            'Talk Show #1 Senior Perspective: "Merajut Masa Depan Cerah Bangsa: Membangun Ketahanan Finansial Keluarga Muda"',
            'Talk Show #2 Perspective Generasi MZ: “Masyarakat Cerdas: Membangun Ketahanan Generasi Muda Melalui Investasi Tepat"',
            "Games",
            "Live Music",
            "Booth Literasi Produk/Kebijakan Lembaga",
            "Live melalui Youtube BI",
        ],
        "color1" => "#6040bb",
        "color2" => "#e1d6ff",
    ],

];

$testimony = [
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214009.png?updatedAt=1727361746459",
        "name" => "Dewi",
        "text" => "Sesi ini membuat saya sadar betapa pentingnya memahami produk keuangan sejak dini.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214044.png?updatedAt=1727361743203",
        "name" => "Andini",
        "text" => "Acara ini sangat membuka wawasan saya tentang pentingnya literasi keuangan. Setiap seri acara memiliki berbagai macam topik yang berbeda.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214146.png?updatedAt=1727361742996",
        "name" => "Rizky Pratama",
        "text" => "Saya merasa lebih percaya diri untuk mulai berinvestasi setelah mengikuti sesi ini. Kolaborasi antar otoritas benar-benar memberikan perspektif yang lengkap.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214155.png?updatedAt=1727361743009",
        "name" => "Linda Yuliana",
        "text" => "Sangat menarik! Saya sekarang lebih mengerti tentang pengelolaan keuangan yang baik.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214137.png?updatedAt=1727361742860",
        "name" => "Bintang",
        "text" => "Konsep acara disusun dengan baik dan sangat menarik untuk generasi muda. Topik talk show juga memberikan tambahan wawasan. ",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214114.png?updatedAt=1727361742830",
        "name" => "Putri Panggabean",
        "text" => "Narasumber yang dihadirkan sangat menarik dan sangat relevan dengan perkembangan ekonomi saat ini.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214124.png?updatedAt=1727361743420",
        "name" => "Citra",
        "text" => "Sangat bermanfaat, saya menjadi lebih mengenal pasar keuangan dan berbagai macam risiko berinvestasi.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214103.png?updatedAt=1727361742904",
        "name" => "Panji P",
        "text" => "Saya jadi lebih paham tentang pentingnya investasi masa depan bagi kami generasi muda sebagai perencanaan keuangan masa depan.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214036.png?updatedAt=1727361742695",
        "name" => "Tito Rahmadi",
        "text" => "Materi yang disampaikan berguna bagi mahasiswa seperti saya yang tertarik dengan instrumen investasi.",
    ],
    [
        "avatar" => "https://ik.imagekit.io/d9hiweoihy/likeit/2024/avatar/iScreen%20Shoter%20-%20Google%20Chrome%20-%20240926214403.png?updatedAt=1727361859192",
        "name" => "Larasati Dewi",
        "text" => "Tidak hanya belajar tentang pilihan investasi, tetapi juga bagaimana menyusun rencana keuangan. ",
    ],
];

?>
<div class="site-index">

    <div class="body-content">

        <div class="d-flex" style="flex-direction: column;">
            <div class="row">
                <div class="col-md-12">
                    <div class="example-marquee">
                        <div id="vbg" data-vbg-mobile="true" data-vbg-load-background="true" data-vbg="https://www.youtube.com/embed/0Cbr-tHpATM?si=ttr6kPO7jC5XKd20"></div>
                        <div class="content">
                            <div class="row inner justify-content-between d-flex flex-column" style="background: #00000075;height: 100vh;">
                                <!-- <h1>Cake in a frying pan? Impossible!</h1> -->
                                <div class="bgv-likeit">
                                    <div class="col-md-12 mb-1">
                                        <img class="navbar-logo" style="width: 100%;max-width: 450px;" src="<?= Yii::getAlias('@web') ?>/images/navbar_logo.png" alt="">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <div class="row align-items-center justify-content-center" style="font-size: 36px;color: white;">
                                            <div class="col-md-auto">6, 7, 8, 9</div>
                                            <div class="col-md-auto">
                                                <img src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Small%20Color%20Circle.png?updatedAt=1719623720088" alt="">
                                            </div>
                                            <div class="col-md-auto">NOVEMBER 2024</div>
                                        </div>

                                        <div style="font-size: 16px;color: white;">
                                            Main Atrium Gandaria City, Jakarta Selatan
                                        </div>
                                    </div>
                                    <div class="col-md-12 ">
                                        <a href="<?= Url::to(['/event/registrasi']) ?>" class="btn btn-main">REGISTRASI DISINI</a>

                                    </div>
                                </div>
                                <div class="detail-arrow" style="    margin-bottom: 100px;">
                                    <a id="play-vid" href="javascript:void(0)" class="btn btn-survey mb-5 fade-out">
                                        Detail Acara <i class="fa fa-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="">
                <div class="col-md-12">
                    <img style="width: 100%;" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/IG%20POST_LIKEIT%202024_ALT%203%20final.jpg?updatedAt=1730785542657" alt="">
                    <!-- <img style="width: 100%;" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Flyer%20Survei%203-10-2024.png?updatedAt=1727962952084" alt=""> -->

                </div>

            </div>
            <div class="row my-0" style="">
                <div class="col-md-12 d-flex justify-content-center m-0">
                    <img class="agenda-logo" src="<?= Yii::getAlias('@web') ?>/images/navbar_logo.png" alt="">
                </div>
                <div class="col-md-12 row row-summary m-0" style="">
                    <?php foreach ($agenda as $key => $value) : ?>
                        <div class="col-md-4">
                            <div class="heading-custom m-0 mb-3">
                                <div><?= $value['title'] ?></div>
                            </div>
                            <div class="card" style="font-size: 20px;">
                                <?php if ($value['soon']) : ?>

                                    <div class="card-title text-center mb-0" style="background: <?= $value['color1'] ?>;color: white;padding: 5px 10px;"><?= $value['time'] ?></div>
                                    <div class="card-body text-2 agenda-soon" style="">
                                        <div class="text-center" style="font-size: 28px;">Coming Soon</div>
                                    </div>
                                <?php else : ?>
                                    <div class="card-title text-center mb-0" style="background: <?= $value['color1'] ?>;color: white;padding: 5px 10px;"><?= $value['time'] ?></div>
                                    <div class="card-body text-2" style="">
                                        <div class="text-center" style="font-size: 28px;height: 90px;"><i class="fa fa-map-pin"></i> <?= $value['place'] ?></div>
                                        <div class="text-center d-flex align-items-center" style="color: <?= $value['color1'] ?>; height: 140px;">
                                            <?= $value['topic'] ?>
                                        </div>
                                        <br>
                                        <ul class="mb-0" style="background: <?= $value['color2'] ?>;color: <?= $value['color1'] ?>; padding: 20px 40px; min-height: 550px;">
                                            <?php foreach ($value['list'] as $key2 => $value2) : ?>
                                                <li><?= $value2 ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>


            </div>
            <div class="row action-button  m-3">
                <div class="col-md-8 col-sm-12  my-2" style="">
                    <h5 class="mb-0"><i class="fas fa-info-circle" aria-hidden="true"></i> Yuk Ikutan Literasi Keuangan Indonesia Terdepan - LIKE It #3</h5>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="/web/index.php/content/like-it-3" class="btn btn-success btn-lg btn-block"><i class="fas fa-arrow-right" aria-hidden="true"></i> Selengkapnya</a>
                </div>
            </div>
            <div class="row my-0" style="margin-bottom: 50px !important;">


                <div class="col-md-12  row align-items-center m-3" style="gap: 10px !important;">
                    <div style="width: 2%;" class=" hidden-sm hidden-xs">
                    </div>
                    <div class="rundown-blue text-center" style="">
                        <div style="font-size: 42px;font-weight: 800;">DAY 1</div>
                        <div style="font-size: 18px;">Rabu, tanggal 6 November 2024 </div>
                        <div style="font-size: 18px;">10.00 – 14.15</div>
                    </div>
                    <div class="rundown-orange flex-column" style="align-items: start;">
                        <div><strong>Grand Ceremony LIKE It 2024 </strong></div>
                        <p>“Generasi Muda Menuju Masa Depan Cerah dengan Berinvestasi di Pasar Keuangan”</p>
                        <p>Talk Show Sesi #1 Senior Perspective: "Merajut Masa Depan Cerah Bangsa: Membangun Ketahanan Finansial Keluarga Muda" </p>
                        <p>Talk Show Sesi #2 Perspective Generasi MZ: “Masyarakat Cerdas: Membangun Ketahanan Generasi Muda Melalui Investasi Tepat" </p>
                    </div>
                    <div style="width: 2%;" class="hidden-sm hidden-xs">
                    </div>
                </div>
                <div class="col-md-12  row align-items-center m-3" style="gap: 10px !important;">
                    <div style="width: 2%;" class=" hidden-sm hidden-xs">
                    </div>
                    <div class="rundown-blue text-center" style="">
                        <div style="font-size: 42px;font-weight: 800;">DAY 2</div>
                        <div style="font-size: 18px;">Kamis, 7 November 2024 </div>
                        <div style="font-size: 18px;">14.00 – 22.00</div>

                    </div>
                    <div class="rundown-orange flex-column" style="align-items: start;gap: 12px;">
                        <div>Talkshow by PT Pegadaian</div>
                        <div>Talkshow by PT Bank Syariah Indonesia (Persero), Tbk.</div>
                        <div>Talkshow by PT Panin Asset Management</div>
                        <div>Talkshow by PT Bank Mandiri (Persero), Tbk.</div>
                    </div>
                    <div style="width: 2%;" class="hidden-sm hidden-xs">
                    </div>
                </div>
                <div class="col-md-12  row align-items-center m-3" style="gap: 10px !important;">
                    <div style="width: 2%;" class=" hidden-sm hidden-xs">
                    </div>
                    <div class="rundown-blue text-center" style="">
                        <div style="font-size: 42px;font-weight: 800;">DAY 3</div>
                        <div style="font-size: 18px;">Jum'at, 8 November 2024 </div>
                        <div style="font-size: 18px;">14.00 – 22.00</div>

                    </div>
                    <div class="rundown-orange flex-column" style="align-items: start;gap: 12px;">
                        <div>Talkshow by Otoritas Jasa Keuangan</div>
                        <div>Talkshow by PT Bank UOB Indonesia</div>
                        <div>Talkshow by Kementerian Keuangan</div>
                        <div>Talkshow by PT Bank Negara Indonesia (Persero), Tbk.</div>
                    </div>
                    <div style="width: 2%;" class="hidden-sm hidden-xs">
                    </div>
                </div>
                <div class="col-md-12  row align-items-center m-3" style="gap: 10px !important;">
                    <div style="width: 2%;" class=" hidden-sm hidden-xs">
                    </div>
                    <div class="rundown-blue text-center" style="">
                        <div style="font-size: 42px;font-weight: 800;">DAY 4</div>
                        <div style="font-size: 18px;">Sabtu, 9 November 2024 </div>
                        <div style="font-size: 18px;">14.00 – 22.00</div>
                    </div>
                    <div class="rundown-orange flex-column" style="align-items: start;gap: 12px;">
                        <div>Talkshow by Bank Indonesia</div>
                        <div>Talkshow by Lembaga Penjamin Simpanan</div>
                    </div>
                    <div style="width: 2%;" class="hidden-sm hidden-xs">
                    </div>
                </div>


            </div>
            <div class="row m-auto align-items-center" style="">
                <div class="col-md-12">
                    <div class="d-flex align-items-center" style="justify-content: center;margin-bottom: 60px;">
                        <div class="card-gradient-1">
                            <div class="card-gradient-1-body">LIKE It SUMMARY</div>
                        </div>
                    </div>
                    <div class="row row-summary" style="">
                        <?php foreach ($event as $key => $value) : ?>
                            <div class="col-md-4">
                                <div>
                                    <iframe width="100%" height="250" src="<?= $value['url'] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                                <div class="card-gradient-3" style="">
                                    <div class="card-gradient-3-body" style="">
                                        <div style="text-align: center;"><?= $value['title'] ?></div>
                                        <div class="" style="font-size: 20px;min-height: 400px;padding: 20px;"><?= $value['description'] ?></div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row my-0" style=" min-height: 600px;position: relative;">
                <div class="col-md-1 justify-content-end hidden-xs hidden-sm" style="display: flex;align-items: start;height: 500px;">

                </div>
                <div class="col-md-10 d-flex align-items-center">
                    <div class="w-100">
                        <div class="d-flex align-items-center" style="justify-content: center;">
                            <div class="card-gradient-2">
                                <div class="card-gradient-2-body">TESTIMONY</div>
                            </div>
                        </div>
                        <div class="owl-carousel owl-theme" style="">
                            <?php foreach ($testimony as $key => $value) : ?>
                                <div class="item d-flex justify-content-center">
                                    <div class="" style="    display: flex;flex-direction: column;align-items: center;;width: 80%;gap: 8px;">
                                        <img style="width: 50%;max-width: 200px;border-radius: 50%;border: 2px solid #e8e8e8;" src="<?= $value['avatar'] ?>" alt="">
                                        <div style="font-weight: 800;font-size: 18px;"><?= $value['name'] ?></div>
                                        <!-- <div>GenBI DIY</div> -->
                                        <p class="" style="text-align: justify;">
                                            <?= $value['text'] ?>
                                        </p>
                                    </div>

                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 justify-content-start hidden-xs hidden-sm" style="display: flex;align-items: end;height: 500px;">
                </div>
                <img class="img-left-2" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" style="border-radius:10px;width:220px;">
                <img class="img-right-2" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" style="border-radius:10px;width:220px;transform: rotate(180deg);">
                <!-- <img style="height: 30px;" class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Garis%20Motif.png?updatedAt=1719623719637" alt=""> -->
                <!-- <div class="w-100" style="height: 30px;background: red;"></div> -->
            </div>
            <!-- <div class="row my-0 row-countdown" style="margin-bottom: 50px !important; height: 560px;position: relative;">
                <div class="col-md-3 hidden-xs hidden-sm  d-flex align-items-end justify-content-end">
                </div>
                <div class="col-md-6" style="display: flex;flex-direction: column;align-items: center; justify-content: center;z-index: 1;">
                    <img class="countdown-img" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Countdown.png?updatedAt=1719624709838" alt="">
                    <div class="countdown-header " style="position: relative;">

                        <div>EVENT COUNTDOWN</div>
                    </div>
                    <div style="padding: 20px;padding: 20px;background: #9a3bb9;border-radius: 20px;">
                        <div class="countdown-body" style="">
                            <div class="flipper" data-reverse="true" data-datetime="2024-11-06 00:00:00" data-template="dd|HH|ii" data-labels="Days|Hours|Minutes|Seconds" id="myFlipper"></div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 hidden-xs hidden-sm d-flex align-items-start justify-content-end">
                </div>
                <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%202%20+%20Orang.png?ik-obj-version=PI0ij9rKT3egtT1qGacTBMvKHVLnxgX.&updatedAt=1719634391136" alt="">

                <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
                <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
            </div> -->
            <div class="row my-0 live-stream-section" style="height: 800px;position: relative;">
                <div class="col-md-1 hidden-xs hidden-sm  d-flex align-items-end justify-content-end">
                </div>
                <div class="col-md-10" style="display: flex;flex-direction: column;align-items: center; justify-content: center;">
                    <div class="countdown-header ">
                        LIVE STREAMING
                    </div>
                    <div style="z-index: 10;padding: 20px;padding: 20px;background: #9a3bb9;border-radius: 20px;width: 100%;height: 80%;">
                        <iframe width="100%" height="100%" style="" src="https://www.youtube.com/embed/TXDfC-Pfnhk?si=PX4Mm9U1MZWUV1X8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-1 hidden-xs hidden-sm d-flex align-items-start justify-content-end">
                </div>
                <img class="img-left" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%202%20+%20Orang.png?ik-obj-version=PI0ij9rKT3egtT1qGacTBMvKHVLnxgX.&updatedAt=1719634391136" alt="">

                <img class="img-right" style="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="">
                <!-- <div class="col-md-12 position-absolute" style="bottom: 0;"> -->
                <img class="img-city" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Gedung-Gedung.png?updatedAt=1719623724320" alt="">
                <!-- </div> -->
            </div>
            <div class="row p-5">
                <div class="col-md-9 mb-3">
                    <!-- <div class="card"> -->
                    <!-- <div class="card-body pb-0"> -->
                    <div class="heading-custom">
                        <div>Social Media LIKE IT</div>
                    </div>
                    <div class="row mb-0 mx-0 mt-3 socmed-bottom" style="justify-content: space-between;">
                        <div class=" mb-3 p-0">
                            <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/instagram_2111463.png?updatedAt=1690541536246" alt="" width="40" height="40"> -->
                                <a target="_blank" href="https://www.bi.go.id">
                                    <span style="font-size: 12pt;display: flex;gap: 5px;">
                                        <i class="fas fa-globe" style="font-size: 25px;" aria-hidden="true"></i>
                                        <strong>Bank indonesia</strong>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class=" mb-3 p-0">
                            <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/instagram_2111463.png?updatedAt=1690541536246" alt="" width="40" height="40"> -->
                                <a target="_blank" href="https://www.instagram.com/bank_indonesia">
                                    <span style="font-size: 12pt;">
                                        <i class="fa-brands fa-2xl fa-instagram" aria-hidden="true"></i>
                                        <strong>Bank indonesia</strong>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class=" mb-3 p-0">
                            <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                <!-- <img src="https://ik.imagekit.io/d9hiweoihy/icons/twitter_3670151.png?updatedAt=1690541536092" alt="" width="40" height="40"> -->
                                <a target="_blank" href="https://twitter.com/bank_indonesia"><span style="font-size: 12pt;"><i class="fa-brands fa-2xl fa-x-twitter" aria-hidden="true"></i><strong>@bank_indonesia</strong></span></a>
                            </div>
                        </div>
                        <div class=" mb-3 p-0">
                            <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                <a target="_blank" href="https://www.youtube.com/channel/UCV7sYsOlkpbHybdIWt7gZbg">
                                    <span style="font-size: 12pt;">
                                        <i class="fa-brands fa-2xl fa-youtube" aria-hidden="true"></i>
                                        <strong>Bank Indonesia</strong>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class=" mb-3 p-0">
                            <div class="d-flex" style="align-items: center;gap: 5px;    justify-content: center;">
                                <a target="_blank" href="https://web.facebook.com/BankIndonesiaOfficial">
                                    <span style="font-size: 12pt;">
                                        <i class="fa-brands fa-2xl fa-facebook" aria-hidden="true"></i>
                                        <strong>Bank Indonesia</strong>
                                    </span>
                                </a>
                            </div>
                        </div>

                    </div>
                    <!-- </div> -->
                    <!-- </div> -->
                </div>
                <div class="col-md-3 mb-3">
                    <!-- <div class="card"> -->
                    <!-- <div class="card-body pb-0"> -->
                    <div class="heading-custom d-flex justify-content-between" style="   align-items: center;">
                        <div style="font-size: 20px;display: flex;align-items: center;padding: 0;height: 57px;justify-content: space-around;">
                            <div>Visitor</div>
                            <div id="visitor" style="font-size: 24px;color: #dd4c2a;"><img style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&amp;C=17&amp;D=0&amp;N=88923&amp;M=0" alt="" border="0"></div>
                        </div>

                    </div>
                    <!-- <div class="mb-4">
                                
                            </div> -->
                    <!-- </div> -->
                    <!-- </div> -->
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    // $('#intromodal').modal()
    $('#visitor').html('<img style="height: 28px;" src="//counter.websiteout.net/compte.php?S=likeit.co.id&C=17&D=0&N=88923&M=0" alt="" border="0" />')

    $('body').css('overflow', 'hidden');
    $(window).scrollTop(0);

    $(window).on('beforeunload', function() {
        $(window).scrollTop(0);
    });

    jQuery('[data-vbg]').youtube_background({
        // 'fit-box':false,
        'offset': 400,
        // 'muted': false,
        autoplay: true,
        // volume: 0.2,
        'always-play': true,
        // lazyloading: true,
        'load-background': true,
        'force-on-low-battery': true,
        // 'play-button': true
    });


    jQuery('#vbg').on('video-background-ready', function(event) {
        // console.log('video-background-ready'); // the video instance object
        // console.log(event.originalEvent.detail.player); // the video instance object
        // $('html, body').animate({ scrollTop: 0 }, 'slow');


        document.getElementById('play-vid').addEventListener('click', function() {
            // alert('Button clicked!');
            $('body').css('overflow', '');

            $('html, body').animate({
                scrollTop: 400
            }, 'slow');

            event.originalEvent.detail.unmute()


            $('#play-vid').hide()
        });

    });


    jQuery('#vbg').on('video-background-play', function(event) {



        // window.addEventListener('scroll', function() {
        //     console.log('Page is scrolling...');
        //     // document.querySelector('#play-vid').click()
        //     event.originalEvent.detail.unmute()
        //     event.originalEvent.detail.play()
        // });

        // setTimeout(() => {
        //     document.querySelector('#play-vid').click()
        // }, 200);

    });

    // video-background-unmute

    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        slideBy: 3,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 3
            }
        }
    });

    jQuery(function($) {
        $('#myFlipper').flipper('init');
    });
</script>