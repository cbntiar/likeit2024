<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100" style="">
  <?php $this->beginBody() ?>
  <?php

  $menus = [
    ['id' => 'menu1', 'label' => 'Home', 'url' => '/site/index', 'children' => [], 'icon' => 'home'],
    // ['id' => 'menu1', 'label' => 'Game Password', 'url' => '/event/quiz', 'children' => []],
    ['id' => 'menu1', 'label' => 'Tentang', 'url' => '', 'children' => [
      ['label' => 'Profil LIKE IT', 'url' => '/content/profile'],
      ['label' => 'Stakeholders LIKE IT', 'url' => '/content/stakeholders'],
      ['label' => 'Social Media LIKE IT', 'url' => '/content/social-media'],
    ]],
    ['id' => 'menu2', 'label' => 'Agenda', 'url' => '', 'children' => [
      ['label' => 'LIKE IT #1', 'url' => '/content/like-it-1'],
      ['label' => 'LIKE IT #2', 'url' => '/content/like-it-2'],
      ['label' => 'LIKE IT #3', 'url' => '/content/like-it-3'],
      // ['label' => 'LIKE IT #4', 'url' => '/content/like-it-4'],
    ]],
    ['id' => 'menu3', 'label' => 'Galeri LIKE IT', 'url' => '', 'children' => [
      ['label' => 'Galeri LIKE IT 2021', 'url' => '/content/galeri-like-it-2021'],
      ['label' => 'Galeri LIKE IT 2022', 'url' => '/content/galeri-like-it-2022'],
      ['label' => 'Galeri LIKE IT 2023', 'url' => '/content/galeri-like-it-2023'],
      // ['label' => 'Galeri LIKE IT 2024', 'url' => '/content/galeri-like-it-2024'],
      ['label' => 'LPS CreaVid 2022', 'url' => '/content/lps-creavid-2022'],
      ['label' => 'Jingle LIKE IT 2021', 'url' => '/content/jingle-like-it'],
    ]],
    ['id' => 'menu4', 'label' => 'Registrasi', 'url' => '/event/registrasi', 'children' => []],
    
  ];

  ?>
  <header>
    <nav class="navbar navbar-scrolled justify-content-between navbar-expand-md">

      <a class="navbar-brand" href="<?= Url::to(['/site/index']) ?>">
        <img class="navbar-logo" width="150px" src="<?= Yii::getAlias('@web') ?>/images/navbar_logo.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <!-- <span class="navbar-toggler-icon"></span> -->
        <i class="fas fa-list" style="color: #fff;"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown" style="justify-content: right;">
        <ul class="navbar-nav">
          <?php foreach ($menus as $key => $menu) : ?>
            <?php if (count($menu['children']) > 0) : ?>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="<?= $menu['id'] ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?= $menu['label'] ?>
                </a>
                <div class="dropdown-menu my-0" aria-labelledby="<?= $menu['id'] ?>">
                  <?php foreach ($menu['children'] as $key => $child) : ?>
                    <a class="dropdown-item" href="<?= Url::to([$child['url']]) ?>"><?= $child['label'] ?></a>
                  <?php endforeach; ?>
                </div>
              </li>
            <?php else : ?>
              <li class="nav-item active">
                <a class="nav-link" href="<?= Url::to([$menu['url']]) ?>">
                  <?php if (!empty($menu['icon'])) : ?>
                    <i class="fas fa-<?= $menu['icon'] ?> mr-2"></i>
                  <?php endif; ?>
                  <?= $menu['label'] ?>
                </a>
              </li>
            <?php endif; ?>
          <?php endforeach; ?>
          <li class="nav-item">
                <a class="nav-link btn btn-survey" target="_blank" href="https://bit.ly/SurveiPraLikeIt2024">
                  Survey Investor
                </a>
              </li>
        </ul>
      </div>
    </nav>

  </header>

  <main role="main" class="flex-shrink-0 main">
    <!-- <div class="container"> -->
    <!-- <img style="position: fixed;width: 100%;height: 100%;object-fit: cover;object-position: center;" src="https://ik.imagekit.io/d9hiweoihy/likeit/background.png?updatedAt=1691083353436" alt=""> -->
    <div class="h-100" style="margin-top: 85px;">
      <!-- <?= Breadcrumbs::widget([
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        <?= Alert::widget() ?> -->
      <?= $content ?>
    </div>
  </main>

  <footer style="margin-top: auto;position: relative;z-index: 1;">
    <img class="" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/Geometric%201.png?updatedAt=1719623724275" alt="" style="position: absolute;width: 100px;right: 0;top: 22px;">
    <img class="hidden-sm hidden-xs" src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/KV_LIKE%20IT%202024_REV%201_4%20(3).png?updatedAt=1727363415940" alt="" style="position: absolute;width: 343px;left: -4px;bottom: 118px;">
    <div class="container">

      <div class="row d-flex justify-content-between " style="border-bottom: 2px solid #ccc6ea !important;">
        <div class="col-12 col-sm-4 col-md-2 col-lg-2 pt-4">
          <div class="row">
            <div class="col d-flex align-items-center">
              <!-- <img style="margin-left: auto; margin-right: auto;" src="https://ik.imagekit.io/d9hiweoihy/likeit/logo.png?updatedAt=1690531886657" alt="Logo LIKE IT" width="250px" height="auto"> -->
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-8 col-md-8 col-lg-7 py-4">
          <div class="row">
            <div class="col-6 col-sm-4">
              <ul style="list-style: none; padding-left: 10px;">
                <li class="mb-4" style="font-size: 20px; font-weight: 600; color: #df0d90;">Navigation</li>
                <li><a href="<?= Url::to(['/site/index']) ?>">Home</a></li>
                <li><a href="<?= Url::to(['/content/profile']) ?>">Profil</a></li>
                <li><a href="<?= Url::to(['/content/stakeholders']) ?>">Stakeholders</a></li>
                <li><a href="<?= Url::to(['/event/registrasi']) ?>">Registrasi</a></li>
              </ul>
            </div>
            <div class="col-6  col-sm-4">
              <ul style="list-style: none; padding-left: 10px;">
                <li class="mb-4" style="font-size: 20px; font-weight: 600; color: #df0d90;">Agenda</li>
                <li><a href="<?= Url::to(['/content/like-it-1']) ?>">LIKE IT #1</a></li>
                <li><a href="<?= Url::to(['/content/like-it-2']) ?>">LIKE IT #2</a></li>
                <li><a href="<?= Url::to(['/content/like-it-3']) ?>">LIKE IT #3</a></li>
                <!-- <li><a href="<?= Url::to(['/content/like-it-4']) ?>">LIKE IT #4</a></li> -->
              </ul>
            </div>
            <div class="col-12  col-sm-4">
              <ul style="list-style: none; padding-left: 10px;">
                <li class="mb-4" style="font-size: 20px; font-weight: 600; color: #df0d90;">Galeri</li>
                <li><a href="<?= Url::to(['/content/galeri-like-it-2021']) ?>">LIKE IT 2021</a></li>
                <li><a href="<?= Url::to(['/content/galeri-like-it-2022']) ?>">LIKE IT 2022</a></li>
                <li><a href="<?= Url::to(['/content/galeri-like-it-2023']) ?>">LIKE IT 2023</a></li>
                <!-- <li><a href="<?= Url::to(['/content/galeri-like-it-2024']) ?>">LIKE IT 2024</a></li> -->
                <li><a href="<?= Url::to(['/content/lps-creavid-2022']) ?>">LPS Creavid 2022</a></li>
                <li><a href="<?= Url::to(['/content/jingle-like-it']) ?>">Jingle LIKE IT 2021</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="row m-0 py-3 w-100">
        <div class="col-12 footer-socmed row d-flex justify-content-between d-flex align-items-center m-0">
          <div style="color: #de0e90;font-weight: 900;font-size: 20px;">&copy <?= date('Y') ?> LIKE IT</div>
          <div class="d-flex justify-content-center" style="gap: 20px;">
            <div class="p-1">
              <a target="_blank" href="https://www.bi.go.id">
                <i class="fas fa-globe" style="font-size: 25px;"></i>
              </a>
            </div>
            <div class="p-1">
              <a target="_blank" href="https://www.instagram.com/bank_indonesia/">
                <i class="fa-brands fa-2xl fa-instagram"></i>
              </a>
            </div>
            <div class="p-1">
              <a target="_blank" href="https://x.com/bank_indonesia">
                <i class="fa-brands fa-2xl fa-x-twitter"></i>
              </a>
            </div>
            <div class="p-1">
              <a target="_blank" href="https://www.youtube.com/channel/UCV7sYsOlkpbHybdIWt7gZbg">
                <i class="fa-brands fa-2xl fa-youtube"></i>
              </a>
            </div>
            <div class="p-1">
              <a target="_blank" href="https://web.facebook.com/BankIndonesiaOfficial">
                <i class="fa-brands fa-2xl fa-facebook"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </footer>

  <?php $this->endBody() ?>
</body>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</html>
<?php $this->endPage() ?>


<script>
  // $(function () {
  //     $(document).scroll(function () {
  //         var $nav = $(".navbar");
  //         $nav.toggleClass('navbar-scrolled', $(this).scrollTop() > $nav.height());
  //     });
  // });
</script>