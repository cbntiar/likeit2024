<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100" style="">
  <?php $this->beginBody() ?>
  <?php

  ?>
  <header>
    <nav class="navbar navbar-scrolled justify-content-between navbar-expand-md">

      <a class="navbar-brand" href="<?= Url::to(['/site/index']) ?>">
        <img class="navbar-logo" width="150px" src="<?= Yii::getAlias('@web') ?>/images/navbar_logo.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <!-- <span class="navbar-toggler-icon"></span> -->
        <i class="fas fa-list" style="color: #fff;"></i>
      </button>
   
    </nav>

  </header>

  <main role="main" class="flex-shrink-0 main">
    <!-- <div class="container"> -->
    <!-- <img style="position: fixed;width: 100%;height: 100%;object-fit: cover;object-position: center;" src="https://ik.imagekit.io/d9hiweoihy/likeit/background.png?updatedAt=1691083353436" alt=""> -->
    <div class="h-100" style="margin-top: 85px;">
     
      <?= $content ?>
    </div>
  </main>

  <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>


<script>
  // $(function () {
  //     $(document).scroll(function () {
  //         var $nav = $(".navbar");
  //         $nav.toggleClass('navbar-scrolled', $(this).scrollTop() > $nav.height());
  //     });
  // });
</script>