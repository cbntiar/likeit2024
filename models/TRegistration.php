<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_registration".
 *
 * @property int $id
 * @property int $event_id
 * @property string $nama_lengkap
 * @property string $no_ktp
 * @property string $email
 * @property string $profesi
 * @property string $instansi
 * @property string $no_hp
 * @property string $description
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $kategori_peserta
 * @property string|null $umur
 * @property string|null $kota_domisili
 */
class TRegistration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'nama_lengkap', 'email', 'profesi', 'description', 'kategori_peserta', 'kota_domisili'], 'required'],
            [['event_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'kategori_peserta', 'kp', 'kpw', 'keterangan', 'preferensi_kehadiran', 'instansi', 'no_hp'], 'safe'],
            [['umur', 'kota_domisili'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'nama_lengkap' => 'Nama Lengkap',
            'no_ktp' => 'No KTP',
            'email' => 'Email',
            'profesi' => 'Profesi/Jabatan',
            'no_hp' => 'No Hp',
            'description' => 'Motivasi mengikuti acara',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'umur' => 'Umur',
            'kategori_peserta' => 'Kategori Peserta',
            'kota_domisili' => 'Kota Domisili saat ini',
            'kp' => 'KP',
            'kpw' => 'KPw',
            'keterangan' => 'Keterangan',
            'preferensi_kehadiran' => 'Preferensi Kehadiran',
        ];
    }
}
