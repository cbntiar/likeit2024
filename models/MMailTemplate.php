<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_mail_template".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $subject
 * @property string|null $template
 * @property string|null $created_at
 */
class MMailTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_mail_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template'], 'string'],
            [['created_at'], 'safe'],
            [['type', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'subject' => 'Subject',
            'template' => 'Template',
            'created_at' => 'Created At',
        ];
    }
}
