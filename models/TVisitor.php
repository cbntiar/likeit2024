<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_visitor".
 *
 * @property int $id
 * @property string $ip
 * @property string $user_agen
 * @property string|null $referer
 * @property string $created_at
 * @property string|null $url
 */
class TVisitor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_visitor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'user_agen', 'created_at'], 'required'],
            [['user_agen', 'referer', 'url'], 'string'],
            [['created_at'], 'safe'],
            [['ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'user_agen' => 'User Agen',
            'referer' => 'Referer',
            'created_at' => 'Created At',
            'url' => 'Url',
        ];
    }
}
