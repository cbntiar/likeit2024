<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "t_certificate_request".
 *
 * @property int $id
 * @property int $event_id
 * @property string $nama_lengkap
 * @property string $no_hp
 * @property string $email
 * @property string $image
 * @property string|null $status
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 * @property string|null $updated_by
 */
class TCertificateRequest extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_certificate_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'nama_lengkap', 'email'], 'required'],
            [['event_id'], 'integer'],
            [['created_at', 'updated_at', 'instansi'], 'safe'],
            [['nama_lengkap', 'no_hp', 'email', 'image', 'status', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'nama_lengkap' => 'Nama Lengkap',
            'no_hp' => 'No Hp',
            'email' => 'Email',
            'image' => 'Foto Bukti Kehadiran',
            'instantsi' => 'Instansi',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
