<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_invite_certificate_request".
 *
 * @property int $id
 * @property string|null $nama_lengkap
 * @property string|null $email
 * @property int|null $status
 */
class TInviteCertificateRequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_invite_certificate_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['nama_lengkap', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_lengkap' => 'Nama Lengkap',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }
}
