<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "belum_kirim".
 *
 * @property string|null $nama_lengkap
 * @property string|null $email
 * @property string|null $instansi
 * @property string|null $status_registrasi
 * @property string|null $flag
 * @property int $id
 */
class BelumKirim extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'belum_kirim';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_lengkap', 'email', 'instansi', 'status_registrasi', 'flag'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nama_lengkap' => 'Nama Lengkap',
            'email' => 'Email',
            'instansi' => 'Instansi',
            'status_registrasi' => 'Status Registrasi',
            'flag' => 'Flag',
            'id' => 'ID',
        ];
    }
}
