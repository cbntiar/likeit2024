<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "t_question".
 *
 * @property int $id
 * @property string $nama_lengkap
 * @property string $no_hp
 * @property string $email
 * @property string $instansi
 * @property string $pertanyaan
 * @property string|null $status
 * @property string|null $jawaban
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 * @property string|null $updated_by
 */
class TQuestion extends \yii\db\ActiveRecord
{
    public $verifyCode, $captcha;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_lengkap', 'email', 'pertanyaan'], 'required'],
            [['pertanyaan', 'jawaban'], 'string', 'max' => 200],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_lengkap', 'no_hp', 'email', 'instansi', 'status', 'created_by', 'updated_by', 'no_hp'], 'string', 'max' => 255],
            // [['email'], 'string', 'max' => 50],
            // ['verifyCode', 'app\components\CaptchaExtendedValidator',
			// 'captchaAction' => Url::to('/site/captcha')]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_lengkap' => 'Nama Lengkap',
            'no_hp' => 'No Hp',
            'email' => 'Email',
            'instansi' => 'Instansi',
            'pertanyaan' => 'Pertanyaan',
            'status' => 'Status',
            'jawaban' => 'Jawaban',
            'created_at' => 'Waktu',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
