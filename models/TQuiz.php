<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_quiz".
 *
 * @property int $id
 * @property string $nama_lengkap
 * @property string $email
 * @property string $no_hp
 * @property string $jawaban
 * @property string|null $created_at
 */
class TQuiz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_quiz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_lengkap', 'email', 'jawaban'], 'required'],
            [['created_at'], 'safe'],
            [['nama_lengkap', 'email', 'no_hp', 'jawaban', 'no_hp'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_lengkap' => 'Nama Lengkap',
            'email' => 'Email',
            'no_hp' => 'No Hp',
            'jawaban' => 'Jawaban',
            'created_at' => 'Created At',
        ];
    }
}
