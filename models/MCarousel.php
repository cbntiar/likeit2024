<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_carousel".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string|null $link
 * @property string $status
 */
class MCarousel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_carousel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'image'], 'required'],
            [['description'], 'string'],
            [['title', 'image', 'link', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'link' => 'Link',
            'status' => 'Status',
        ];
    }
}
