<?php

namespace app\components;


class Helpers {

    private static $key = 'likeit2023';
    
    public static function inputIcon($icon = '', $required = '') {

        $prefix = '';
        if ($icon != '') {
            $prefix = '<div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-'.$icon.'"></i></span>
            </div>';
        }

        return '
        {label} '.$required.'
        <div class="input-group mb-3">
            '.$prefix.'
            {input}
        </div>
        {hint}{error}
        ';
    }

    public static function encrypt($value) {

        return openssl_encrypt($value, "AES-128-ECB", self::$key);
    }

    public static function decrypt($value) {

        return openssl_decrypt($value, "AES-128-ECB", self::$key);
    }

    
    public static function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
 
        if(filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }


}

