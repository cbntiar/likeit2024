<?php
date_default_timezone_set('Asia/Jakarta');

error_reporting(0);
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'prod');

function encrypt($data, $key = 'likeit#2024') {
    $iv = substr(hash('sha256', 'fixed_iv'), 0, 16); // IV tetap (harus 16 byte)
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $key, 0, $iv);
    return base64_encode($encrypted);
}

function decrypt($data, $key = 'likeit#2024') {
    $iv = substr(hash('sha256', 'fixed_iv'), 0, 16); // IV tetap (harus 16 byte)
    $encrypted_data = base64_decode($data);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
}

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
