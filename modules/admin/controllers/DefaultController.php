<?php

namespace app\modules\admin\controllers;

use app\models\MContent;
use app\models\MContentSearch;
use app\models\TCertificateRequest;
use app\models\TCertificateRequestSearch;
use app\models\TInviteCertificateRequest;
use app\models\TQuestion;
use app\models\TQuestionSearch;
use app\models\TQuiz;
use app\models\TRegistration;
use app\models\TRegistrationSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Mpdf\Mpdf;
use yii\helpers\Url;

/**
 * DefaultController implements the CRUD actions for MContent model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MContent models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'main';

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }

        $register = TRegistration::find()->count();
        $question = TQuestion::find()->count();
        $cert = TCertificateRequest::find()->count();
       
        return $this->render('index', [
            'register' => $register,
            'question' => $question,
            'cert' => $cert,
        ]);
    }

    public function actionContent()
    {
        $this->layout = 'main';

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        
        $searchModel = new MContentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('content', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegistrasi()
    {
        $this->layout = 'main';

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        
        $searchModel = new TRegistrationSearch();
        // $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider = TRegistration::find()->all();

        return $this->render('registrasi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewRegistrasi($id)
    {
        $model = TRegistration::findOne(['id' => $id]);

        return $this->renderPartial('_view_registrasi', [
            'model' => $model,
        ]);
    }

    public function actionViewPertanyaan($id)
    {
        $model = TQuestion::findOne(['id' => $id]);

        return $this->renderPartial('_view_pertanyaan', [
            'model' => $model,
        ]);
    }

    public function actionPilihRegistrasi($id, $type = 1)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $data = [];

        $model = TRegistration::findOne(['id' => $id]);
        $model->status = $type;
        if ($model->save()) {
            $data = [
                'success' => true,
            ]; 
        } else {
            $data = [
                'success' => false,
            ]; 
        }

        return $data;
    }

    public function actionPertanyaan()
    {
        $this->layout = 'main';

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        
        $searchModel = new TQuestionSearch();
        // $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider = TQuestion::find()->all();

        return $this->render('pertanyaan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSertifikat()
    {
        $this->layout = 'main';

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        
        $searchModel = new TCertificateRequestSearch();
        // $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider = TCertificateRequest::find()->all();

        return $this->render('sertifikat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionQuiz()
    {
        $this->layout = 'main';

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        
        // $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider = TQuiz::find()->all();

        return $this->render('quiz', [
            'dataProvider' => $dataProvider,
        ]);
    }

    

    /**
     * Displays a single MContent model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'main';

       
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MContent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $this->layout = 'main';

        $model = new MContent();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = 'main';

        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return MContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MContent::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSendCertificateRequest()
    {
        $model = new TCertificateRequest();

        $post = Yii::$app->request->post();
        if ($post) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            try {
                $model = TCertificateRequest::find()->where(['email' => encrypt($post['TCertificateRequest']['email'])])->one();

                if (empty($model)) {
                    throw new \Exception("Data peserta tidak ditemukan", 1);
                }

                $file = UploadedFile::getInstanceByName('TCertificateRequest[file]');
                if (empty($file)) {
                    throw new \Exception("File pdf tidak ditemukan", 1);
                }
    
                $send = Yii::$app->mailer->compose()
                ->setFrom('no-reply@likeit.co.id')
                ->setTo($post['TCertificateRequest']['email'])
                ->setSubject("Sertifikat Kehadiran Event LIKEIT 2023")
                ->setHtmlBody($this->templateEmailSertifikat($post['TCertificateRequest']['nama_lengkap']))
                ->attach($file->tempName, [
                    'fileName' => 'CERT_LIKEIT_2023_'.strtoupper($post['TCertificateRequest']['nama_lengkap']).'.pdf'
                ])
                ->send();
    
                if ($send) {

                    $model->status = 1;
                    $model->save(false);

                    return [
                        'success' => true,
                        'message' => 'Sertifikat berhasil dikirim'
                    ];
                } else {
                    throw new \Exception("Gagal mengirim email", 1);
                }
            } catch (\Throwable $th) {
                return [
                    'success' => false,
                    'message' => $th->getMessage()
                ];
            }
            
         
        }
        
        return $this->render('kirim_sertifikat', [
            'model' => $model
        ]);
    }

    public function templateEmailSertifikat($nama_lengkap = '', $link = '#')
    {
        $template = '
       
                <div>
                <img style="margin-left: auto; margin-right: auto;" src="https://ik.imagekit.io/d9hiweoihy/likeit/logo.png?updatedAt=1690531886657" alt="Logo Like It" width="100%" height="auto">
                </div>
                <div style="text-align: left; margin-bottom: 30px; font-size: 14pt;">
                <strong>Kepada : </strong> <span style="color: color(srgb 0.0362 0.2989 0.5513)">'.$nama_lengkap.'</span>
                    </div>
                    <div style="text-align: center; margin-bottom: 30px; font-size: 14pt;">
                Terima kasih atas partisipasi Anda di <strong>LIKE IT 2024</strong>
                </div>
                <div style="text-align: center; margin-bottom: 20px; font-size: 15pt;">
                Silakan akses sertifikat anda pada tautan berikut:
                </div>
                    <div style="text-align: center; margin-bottom: 40px; font-size: 15pt;">
                    <a style="color: #1ea3eb" href="'.$link.'" target="_blank">'.$link.'</a>
                </div>
                <div style="text-align: center; margin-bottom: 40px; font-size: 15pt;">
                <div style="margin-bottom: 20px; font-size: 15pt; font-weight: bold;">
                    Salam LIKE IT! 2024
                </div>
                </div>
                <div style="margin-top: 60px; margin-bottom: 0px">
                <img src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/IG%20POST_LIKEIT%202024_ALT%203%20final.jpg?updatedAt=1730785542657" alt="Flyer" width="100%" height="auto">
                </div>
                <div style="padding: 10px; background-color: #fefef3; color: #de0e90">
                <div style="width: 200px; text-align: left; float: left;">
                    <div style="margin-bottom: 20px">Salam LIKE IT!</div>
                    <div style="margin-bottom: 10px">
                    &copy; 2024 <a style="color: #1ea3eb" href="https://likeit.co.id" target="_blank">likeit.co.id</a>
                    </div>
                </div>
                <div style="width: 350px; text-align: right; float: right;">
                    <div style="margin-bottom: 20px">Tautan Social Media</div>
                    <div style="margin-bottom: 10px; color: #de0e90;">
                    <a style="color: #1ea3eb" href="https://www.bi.go.id/">Bank Indonesia</a>
                    <a style="color: #1ea3eb" href="https://www.instagram.com/bank_indonesia/">Instagram</a>
                    <a style="color:#1ea3eb" href="https://x.com/bank_indonesia">X</a>
                    
                    <a style="color: #1ea3eb" href="https://www.youtube.com/channel/UCV7sYsOlkpbHybdIWt7gZbg">Youtube</a>
                        <a style="color: #1ea3eb" href="https://www.facebook.com/BankIndonesiaOfficial">Facebook</a>
                    
                    </div>
                </div>
           
        ';

        return $template;
    }

    public function templateEmailInviteSertifikat($nama_lengkap = '')
    {
        $template = '
        <html>
            <head>
                <title>Upload Kehadiran Sertifikat LIKE IT 2024</title>
            </head>
            <body style="width: 700px; margin: auto;">
                <div>
                <img style="margin-left: auto; margin-right: auto;" src="https://ik.imagekit.io/d9hiweoihy/likeit/logo.png?updatedAt=1690531886657" alt="Logo Like It" width="100%" height="auto">
                </div>
                <div style="text-align: left; margin-bottom: 30px; font-size: 14pt;">
                <strong>Kepada : </strong> <span style="color: color(srgb 0.0362 0.2989 0.5513)">'.$nama_lengkap.'</span>
                    </div>
                    <div style="text-align: center; margin-bottom: 30px; font-size: 14pt;">
                Terima kasih atas partisipasi Anda di <strong>LIKE IT 2024</strong>
                </div>
                <div style="text-align: center; margin-bottom: 20px; font-size: 15pt;">
                Silakan mengisi form sertifikat dan meng-<em>upload</em> bukti kehadiran Anda pada link berikut sebelum <strong>18 November 2024</strong>
                </div>
                    <div style="text-align: center; margin-bottom: 40px; font-size: 15pt;">
                    <a style="color: #1ea3eb" href="https://likeit.co.id/web/event/certificate-request" target="_blank">https://likeit.co.id/web/event/certificate-request</a>
                </div>
                <div style="text-align: center; margin-bottom: 40px; font-size: 15pt;">
                <div style="margin-bottom: 20px; font-size: 15pt; font-weight: bold;">
                    Salam LIKE IT! 2024
                </div>
                </div>
                <div style="margin-top: 60px; margin-bottom: 0px">
                <img src="https://ik.imagekit.io/d9hiweoihy/likeit/2024/IG%20POST_LIKEIT%202024_ALT%203%20final.jpg?updatedAt=1730785542657" alt="Flyer" width="100%" height="auto">
                </div>
                <div style="padding: 10px; background-color: #fefef3; color: #de0e90">
                <div style="width: 200px; text-align: left; float: left;">
                    <div style="margin-bottom: 20px">Salam LIKE IT!</div>
                    <div style="margin-bottom: 10px">
                    &copy; 2024 <a style="color: #1ea3eb" href="https://likeit.co.id" target="_blank">likeit.co.id</a>
                    </div>
                </div>
                <div style="width: 350px; text-align: right; float: right;">
                    <div style="margin-bottom: 20px">Tautan Social Media</div>
                    <div style="margin-bottom: 10px; color: #de0e90;">
                    <a style="color: #1ea3eb" href="https://www.bi.go.id/">Bank Indonesia</a>
                    <a style="color: #1ea3eb" href="https://www.instagram.com/bank_indonesia/">Instagram</a>
                    <a style="color:#1ea3eb" href="https://x.com/bank_indonesia">X</a>
                    
                    <a style="color: #1ea3eb" href="https://www.youtube.com/channel/UCV7sYsOlkpbHybdIWt7gZbg">Youtube</a>
                        <a style="color: #1ea3eb" href="https://www.facebook.com/BankIndonesiaOfficial">Facebook</a>
                    
                    </div>
                </div>
                <div style="clear: both;"></div>
                </div>
            </body>
            </html>
        ';

        return $template;
    }

    public function actionBlastInviteCertificate()
    {
        $this->layout = 'main';

        if ($this->request->isPost) {

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model = TInviteCertificateRequest::find()->where(['status' => null])->orWhere(['status' => 0])->one();
            // $model = TInviteCertificateRequest::find()->where(['email' => 'emailsuciku@gmail.com'])->one();

            try {
                
                if (!empty($model)) {
    
                    $email = $model->email;
    
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $send = Yii::$app->mailer->compose()
                        ->setFrom('no-reply@likeit.co.id')
                        ->setTo($email)
                        ->setSubject("Upload Kehadiran Sertifikat LIKE IT 2024")
                        ->setHtmlBody($this->templateEmailInviteSertifikat($model->nama_lengkap))
                        ->send();
    
                        if ($send) {
                            $model->status = 1;
                            $model->save(false);
    
                            return [
                                'success' => true,
                                'message' => $email. ': terkirim.',
                                'data' => [
                                    "continue" => true,
                                    "time" => date('Y-m-d H:i:s')
                                ],
                            ];
                        } else {
                            return [
                                'success' => false,
                                'message' => $email. ': gagal mengirim.',
                                'data' => [
                                    "continue" => true
                                ],
                            ];
                        }
                    } else {
                        $model->status = 2;
                        $model->save(false);
    
                        return [
                            'success' => false,
                            'message' => $email. ': gagal mengirim.',
                            'data' => [
                                "continue" => true
                            ],
                        ];
                    }
    
                } else {
                    return [
                        'success' => false,
                        'message' => 'Data tidak ditemukan.',
                        'data' => [
                            "continue" => false
                        ],
                    ];
                }
            } catch (\Throwable $e) {

                $model->status = 2;
                $model->save(false);

                return [
                    'success' => false,
                    'message' => 'Internal server error: '.$model->email.' - '.$e->getMessage(),
                    'data' => [
                        "continue" => true
                    ],
                ];
            }

            
        }


        $summary = Yii::$app->db->createCommand('SELECT
            sum(case when (status = 0 or status is null) then 1 else 0 end) as not_sent,
            sum(case when status = 1 then 1 else 0 end) as sent,
            sum(1) as total
            from t_invite_certificate_request')->queryOne();


        return $this->render('blast-invite-certification', [
            "summary" => $summary,
        ]);
        
    }

    public function actionBlastCertificate()
    {
        $this->layout = 'main';

        set_time_limit(30);
        
        if ($this->request->isPost) {

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            // $model = TCertificateRequest::find()->where(['status' => null])->orWhere(['status' => 0])->one();
            $model = TCertificateRequest::find()->where(['status' => 0])->one();

            if (!empty($model)) {

                // $nama = $this->shortenName($model->nama_lengkap);

                // $pdf = $this->generateCertificatePdf($nama, $model->instansi);

                // if ($pdf) {
                //     $send = $this->sendEmail($nama, $model->email, $pdf, $model);

                //     return $send;
                // }

                // =========================

                // $ch = curl_init();

                // curl_setopt($ch, CURLOPT_URL, 'https://aplikasi.kirim.email/api/v3/transactional/messages');
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // curl_setopt($ch, CURLOPT_POST, 1);
                // $post = array(
                //     'from' => 'no-reply@likeit.co.id',
                //     'to' => $model->email,
                //     'subject' => 'Sertifikat LIKEIT 2024',
                //     'text' => $this->templateEmailSertifikat($model->nama_lengkap, 'https://likeit.co.id/web/index.php/event/get-certificate?e_id='.encrypt($model->email))
                // );
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                // curl_setopt($ch, CURLOPT_USERPWD, 'api' . ':' . '954f738508600ec1ac79dbedfbe729eec593b052f34b3375916ed25b19fefdbb');

                // $headers = array();
                // $headers[] = 'Domain: likeit.co.id';
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                // $result = curl_exec($ch);
                // if (curl_errno($ch)) {
                //     echo 'Error:' . curl_error($ch);
                // }
                // curl_close($ch);

                // ========================

                $api_token = 'f41923700eff0c18648ff88a335a1c87'; //silahkan copy dari api token mailketing
                $from_name = 'LIKEIT 2024'; //nama pengirim
                $from_email = 'no-reply@likeit.co.id'; //email pengirim
                $subject = 'Sertifikat LIKEIT 2024'; //judul email
                $content = $this->templateEmailSertifikat($model->nama_lengkap, 'https://likeit.co.id/web/index.php/event/get-certificate?e_id='.encrypt($model->id)); //isi email format text / html
                $recipient = $model->email; //penerima email
                $params = [
                    'from_name' => $from_name,
                    'from_email' => $from_email,
                    'recipient' => $recipient,
                    'subject' => $subject,
                    'content' => $content,
                    // 'attach1' => 'direct url file httxxx/xxx/xx.pdf',
                    // 'attach2' => 'direct url file httxxx/xxx/xx.pdf',
                    // 'attach2' => 'direct url file httxxx/xxx/xx.pdf',
                    'api_token' => $api_token
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://api.mailketing.co.id/api/v1/send");
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec ($ch); 
                curl_close ($ch);


                $res_decode = json_decode($result);

                if ($res_decode->status == 'success') {
                    $model->status = 1;
                    $model->save(false);

                    return [
                        'success' => true,
                        'message' => $model->email. ': terkirim | '.$res_decode->message.'',
                        'data' => [
                            "continue" => true,
                            "time" => date('Y-m-d H:i:s')
                        ],
                    ];
                } else {
                    return [
                        'success' => false,
                        'message' => $model->email. ': gagal mengirim | '.$res_decode->message.'',
                        'data' => [
                            "continue" => true
                        ],
                    ];
                }

            } else {
                return [
                    'success' => false,
                    'message' => 'Data tidak ditemukan.',
                    'data' => [
                        "continue" => false
                    ],
                ];
            }
        }


        $summary = Yii::$app->db->createCommand('SELECT
            sum(case when (status = 0 or status is null) then 1 else 0 end) as not_sent,
            sum(case when status = 1 then 1 else 0 end) as sent,
            sum(1) as total
            from t_certificate_request where status != 22')->queryOne();


        return $this->render('blast-certification', [
            "summary" => $summary,
        ]);
        
    }

    function shortenName($fullName) {
        $names = explode(' ', $fullName); // Memecah nama menjadi array
        $shortenedName = [];
        
        // Ambil 3 kata pertama
        for ($i = 0; $i < min(4, count($names)); $i++) {
            $shortenedName[] = $names[$i];
        }
        
        // Ambil huruf pertama dari kata ke-4 dan seterusnya
        for ($i = 4; $i < count($names); $i++) {
            $shortenedName[] = substr($names[$i], 0, 1).'.'; // Ambil huruf pertama
        }
        
        // Gabungkan nama yang sudah diproses
        return implode(' ', $shortenedName);
    }

    public function generateCertificatePdf($nama, $instansi = "")
    {
        try {


            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];


            $mpdf = new Mpdf([
                'orientation' => 'L',
                'fontDir' => array_merge($fontDirs, [
                    Yii::getAlias('@webroot/fonts/'),
                ]),
                'fontdata' => $fontData + [ 
                    'pinyonscript' => [
                        'R' => 'PinyonScript-Regular.ttf',
                    ],
                    'tt-chocolates' => [
                        'R' => 'TT Chocolates Bold Italic.ttf',
                    ]
                ],

            ]);

            // echo '<pre>';
            // print_r($mpdf->fontdata);
            // echo '</pre>';
            // exit;

           
            $pageCount = $mpdf->setSourceFile(Yii::getAlias('@webroot/ATTENDACE #1 Back.pdf'));
            $templatePage = $mpdf->importPage(1); // Ambil halaman pertama dari template
            $mpdf->useTemplate($templatePage);

           
            $namaStyle = 'font-family: PinyonScript; font-size: 70px; color: #DD4C2A; text-align: center; position: absolute; top: 320px; width: 100%'; 
            $mpdf->WriteFixedPosHTML( "<div style='$namaStyle'>$nama</div>", 0, 0, 500, 500,);

            $instansiStyle = 'font-family: TT-Chocolates; font-size: 28px; color: #1E3653; text-align: center; position: absolute; top: 425px; width: 100%';
            $mpdf->WriteFixedPosHTML( "<div style='$instansiStyle'>dari $instansi</div>", 0, 0, 500, 500,);
    
            $pdfContent = $mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN);

            // $filename = "Sertifikat_{$nama}.pdf";
        
            // return $mpdf->Output($filename, \Mpdf\Output\Destination::DOWNLOAD); 
            return $pdfContent;
        } catch (\Exception $e) {

            Yii::error("Gagal membuat PDF: " . $e->getMessage());

            return [
                'success' => false,
                'message' => 'Error generate pdf.: '. $e->getMessage(),
                'data' => [
                    "continue" => false
                ],
            ];;
        }
    }

    public function sendEmail($nama, $email, $pdfContent, $model)
    {
        try {
            // $send = Yii::$app->mailer->compose()
            //     ->setFrom('no-reply@likeit.co.id')
            //     ->setTo($email)
            //     ->setSubject("Sertifikat Partisipasi Event")
            //     ->setTextBody("Terima kasih telah berpartisipasi. Sertifikat partisipasi terlampir.")
            //     ->attachContent($pdfContent, [
            //         'fileName' => 'Sertifikat.pdf',
            //         'contentType' => 'application/pdf'
            //     ])
            //     ->send();

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $send = Yii::$app->mailer->compose()
                ->setFrom('no-reply@likeit.co.id')
                ->setTo($email)
                ->setSubject("Sertifikat Kehadiran Event LIKE IT 2024")
                ->setHtmlBody($this->templateEmailSertifikat($nama))
                ->attachContent($pdfContent, [
                    'fileName' => 'Sertifikat LIKE IT 2024 - '.str_replace('.', '', $nama).'.pdf',
                    'contentType' => 'application/pdf'
                ])
                ->send();

                if ($send) {
                    $model->status = 1;
                    $model->save(false);

                    return [
                        'success' => true,
                        'message' => $email. ': terkirim.',
                        'data' => [
                            "continue" => true,
                            "time" => date('Y-m-d H:i:s')
                        ],
                    ];
                } else {
                    return [
                        'success' => false,
                        'message' => $email. ': gagal mengirim.',
                        'data' => [
                            "continue" => true
                        ],
                    ];
                }
            } else {
                return [
                    'success' => false,
                    'message' => $email. ': gagal mengirim.',
                    'data' => [
                        "continue" => true
                    ],
                ];
            }
                
        } catch (\Exception $e) {

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => [
                    "continue" => false
                ],
            ];
        }
    }


    public function actionTestCertificate() {
        $nama = "Adhib Fuad";
        $email = "adhibfuad@gmail.com";

        $pdf = $this->generateCertificatePdf($nama, "Test Universitas");

        if ($pdf) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $send = Yii::$app->mailer->compose()
                ->setFrom('no-reply@likeit.co.id')
                ->setTo($email)
                ->setSubject("Sertifikat Kehadiran Event LIKE IT 2024")
                ->setHtmlBody($this->templateEmailSertifikat($nama))
                ->attachContent($pdf, [
                    'fileName' => 'Sertifikat LIKE IT 2024 - '.$nama.'.pdf',
                    'contentType' => 'application/pdf'
                ])
                ->send();

                var_dump($send);
                exit;
            }

        }
    }
}
