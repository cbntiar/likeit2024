<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>
<?php

$menus = [
    ['id' => 'menu1', 'label' => 'Dashboard', 'url' => '/admin/default/index', 'children' => []],
    ['id' => 'menu1', 'label' => 'Registrasi', 'url' => '/admin/default/registrasi', 'children' => []],
    ['id' => 'menu1', 'label' => 'Pertanyaan', 'url' => '/admin/default/pertanyaan', 'children' => []],
    ['id' => 'menu1', 'label' => 'Sertifikat', 'url' => '/admin/default/sertifikat', 'children' => []],
    ['id' => 'menu1', 'label' => 'Quiz', 'url' => '/admin/default/quiz', 'children' => []],
    // ['id' => 'menu1', 'label' => 'Content', 'url' => '/admin/default/content', 'children' => []],
];

?>
<header>
<nav class="navbar navbar-scrolled justify-content-between navbar-expand-md">
  <a class="navbar-brand" href="#">
    <img class="navbar-logo" width="150px" src="<?= Yii::getAlias('@web') ?>/images/navbar_logo.png" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <!-- <span class="navbar-toggler-icon"></span> -->
    <i class="fas fa-list" style="color: #db6723;"></i>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown" style="justify-content: right;">
    <ul class="navbar-nav">
        <?php foreach ($menus as $key => $menu) : ?>
            <?php if (count($menu['children']) > 0) : ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="<?= $menu['id'] ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $menu['label'] ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="<?= $menu['id'] ?>">
                    <?php foreach ($menu['children'] as $key => $child) : ?>
                        <a class="dropdown-item" href="<?= Url::to([$child['url']]) ?>"><?= $child['label'] ?></a>
                    <?php endforeach; ?>
                    </div>
                </li>
            <?php else : ?>
                <li class="nav-item active">
                    <a class="nav-link" href="<?= Url::to([$menu['url']]) ?>"><?= $menu['label'] ?></a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if (!Yii::$app->user->isGuest) : ?>
            <li class="nav-item active">
                <a class="nav-link" href="<?= Url::to(['/site/logout']) ?>">Logout</a>
            </li>
        <?php endif; ?>
    </ul>
  </div>
</nav>

</header>

<main role="main" class="flex-shrink-0 main" style=" ">
<!-- <img style="position: fixed;width: 100%;height: 100%;object-fit: cover;object-position: center;" src="https://ik.imagekit.io/d9hiweoihy/likeit/background-3?updatedAt=1690814636384" alt=""> -->
    <div class="" style="margin-top: 120px;">
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; LIKEIT <?= date('Y') ?></p>
    </div>
    
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<script>
    // $(function () {
    //     $(document).scroll(function () {
    //         var $nav = $(".navbar");
    //         $nav.toggleClass('navbar-scrolled', $(this).scrollTop() > $nav.height());
    //     });
    // });
</script>