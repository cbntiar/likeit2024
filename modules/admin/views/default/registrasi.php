<?php

use app\models\MCOntent;
use app\models\TRegistration;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\MContentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Data Registrasi';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- DataTables CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<!-- DataTables Buttons CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<!-- jQuery -->
<!-- DataTables JS -->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<!-- DataTables Buttons JS -->
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>

<!-- DataTables FixedColumns CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/4.2.1/css/fixedColumns.dataTables.min.css">

<!-- DataTables FixedColumns JS -->
<script src="https://cdn.datatables.net/fixedcolumns/4.2.1/js/dataTables.fixedColumns.min.js"></script>

<div class="main-content">
    <div class="container mb-5">
        <div class="card bg-white shadow">
          
            <div class="card-body">
                <div class="card-title">
                Registrasi
                </div>
                <table id="example" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Kategori Peserta</th>
                            <th>KP</th>
                            <th>KPw</th>
                            <th>Keterangan</th>
                            <th>Instansi</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>No. HP</th>
                            <th>Profesi</th>
                            <th>Kota</th>
                            <th>Minat</th>
                            <th>Preferensi Kehadiran</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataProvider as $key => $model) : ?>
                            <tr>
                                <td><?= $model->created_at ?></td>
                                <td><?= $model->kategori_peserta ?></td>
                                <td><?= $model->kp ?></td>
                                <td><?= $model->kpw ?></td>
                                <td><?= $model->keterangan ?></td>
                                <td><?= $model->instansi ?></td>
                                <td><?= decrypt($model->nama_lengkap) ?></td>
                                <td><?= decrypt($model->email) ?></td>
                                <td><?= decrypt($model->no_hp) ?></td>
                                <td><?= $model->profesi ?></td>
                                <td><?= $model->kota_domisili ?></td>
                                <td><?= $model->description ?></td>
                                <td><?= $model->preferensi_kehadiran ?></td>
                                <td><?= $model->status ?></td>
                                <td>
                                    <button class="btn btn-success btn-sm" onclick="kirimUndangan('<?= $model->email ?>', this, 0)">Kirim</button>
                                    <button class="btn btn-danger btn-sm" onclick="kirimUndangan('<?= $model->email ?>', this, 1)">Kirim Ulang</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        <!-- Tambahkan data lainnya di sini -->
                    </tbody>
                </table>

               
            </div>

        </div>
    </div>

    <div class="modal" id="modal-detail" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Peserta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-y: auto;">

                </div>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                fixedColumns: {
                    rightColumns: 1, // Membekukan 1 kolom di sebelah kanan (kolom Action)
                    leftColumns: 0
                },
                dom: 'Bfrtip',
                scrollX: true, // Enable horizontal scrolling
                buttons: [{
                    extend: 'excelHtml5',
                    text: 'Export to Excel',
                    title: 'Data Registrasi',
                    exportOptions: {
                        // columns: ':visible'
                        columns: ':not(:last-child)'
                    }
                }],
                className: "compact"
            });
        });


        function viewPeserta(id) {
            $('#modal-detail').modal();
            getPeserta(id)

        }

        function kirimUndangan(email, el, reminder) {

            if (confirm("Kirim?") == true) {
                $.ajax({
                    url: '<?= Url::to(['/event/kirim-undangan']) ?>?email=' + email + '&reminder=' + reminder,
                    type: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $(el).attr('disabled', true)
                    },
                    success: function(response) {
                        alert(response.message)
                        $(el).attr('disabled', false)
                    },
                    complete: function() {
                        $(el).attr('disabled', false)
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                        alert(pesan);
                    }
                });
            } else {
                text = "Batal mengirim";
            }
        }

        function pilihPeserta(id, type) {

            $.ajax({
                url: `<?= Url::to(['/admin/default/pilih-registrasi']) ?>?id=${id}&type=${type}`,
                type: 'GET',
                dataType: 'JSON',
                beforeSend: function() {
                    $('#modal-detail .modal-body').html('<center>Loading Data...</center>');
                },
                success: function(response) {
                    getPeserta(id)
                    $.pjax({
                        container: '#pjax-grid-view'
                    })
                },
                complete: function() {
                    $('#modal-detail').modal('hide');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                    $('#modal-detail .modal-body').html(pesan);
                    // alert(pesan);
                }
            });
        }


        async function blastReminderUndangan(list = []) {

            var total = list.length
            var sent = 0

            for (let index = 0; index < list.length; index++) {
                const email = list[index];

                await $.ajax({
                    url: '<?= Url::to(['/event/kirim-undangan']) ?>?email=' + email + '&reminder=1',
                    type: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        console.log(`sending to ${email}`)
                    },
                    success: function(response) {
                        sent++
                        console.log({
                            email,
                            status: response.success
                        })
                    },
                    complete: function() {},
                    error: function(xhr, ajaxOptions, thrownError) {
                        var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                    }
                });
            }

            return `total ${total} | sent ${sent}`
        }
    </script>