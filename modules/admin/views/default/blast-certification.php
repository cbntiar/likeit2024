<?php

use yii\helpers\Url;

$this->title = 'Blast Certificate'
?>
<div class="main-content h-100" style="position: relative;">
    <div class="container mb-5">
        <h3><?= $this->title ?></h3>
        <div class="row align-items-center" style="gap: 0px !important;">
            <div class="col-md-9">
                <div>Terkirim <span id="sent-count"><?= $summary['sent'] ?></span>/<?= $summary['total'] ?></div>
            </div>
            <div class="col-md-3 text-right d-flex align-items-center justify-content-end" style="gap: 10px;">
                <button id="start-button" class="btn btn-primary">Mulai Proses</button>
                <button id="stop-button" class="btn btn-danger" disabled>Stop Proses</button>
            </div>
            <div class="col-md-12">
                <div id="monitoring" style="height: 300px; overflow: auto; padding: 20px;background: #e3e3e3;margin-top: 10px;border-radius: 10px;"></div>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        let start = true;

        function hitApi() {

            if (!start) return;

            $.ajax({
                url: '<?= Url::to(['/admin/default/blast-certificate']) ?>', // Ganti dengan URL API Anda
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    // Append response ke dalam #monitoring
                    $('#monitoring').prepend(`<div>${JSON.stringify(response)}</div>`);

                    if (response.success == true) {
                        $('#sent-count').html(parseInt($('#sent-count').html()) + 1)
                    }

                    // Jika response mengandung continue: true, lakukan request lagi
                    if (response.data.continue === true) {
                        hitApi(); // Panggil fungsi lagi untuk request berikutnya
                    } else {
                        $('#monitoring').append('<div>Proses selesai.</div>');
                        $('#start-button').prop('disabled', false);
                        $('#stop-button').prop('disabled', true);

                    }
                },
                error: function(error) {
                    $('#monitoring').append('<div>Terjadi kesalahan pada request.</div>');
                    $('#start-button').prop('disabled', false); 
                }
            });
        }

        $('#start-button').click(function() {
            $(this).prop('disabled', true);
            $('#stop-button').prop('disabled', false);

            $('#monitoring').empty(); // Kosongkan monitoring sebelum memulai proses
            start = true; // Set flag ke true untuk memulai proses
            hitApi(); // Mulai proses request
        });

        // Event handler untuk menghentikan proses ketika tombol "Stop" diklik
        $('#stop-button').click(function() {
            $(this).prop('disabled', true);
            $('#start-button').prop('disabled', false);
            start = false; // Set flag ke false untuk menghentikan proses
            $('#monitoring').append('<div>Proses dihentikan.</div>');
        });
    });
</script>