<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\MCOntent $model */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Mc Ontents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="main-content">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class="">View Content</h1>
            </div>
            <div class="card-body">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'type',
                    'content:ntext',
                    'created_at',
                    'created_by',
                    'updated_at',
                    'updated_by',
                    'views',
                ],
            ]) ?>
            </div>
        </div>

    </div>
</div>

