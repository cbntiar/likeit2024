<?php

use app\models\MCOntent;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\MContentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Content Management';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-content">
    <div class="container mb-5">
        <div class="row">
            <div class="col-md-4">
                <div class="small-box bg-color-6" style="color: white;">
                    <div class="inner">
                        <h1><?= $register ?></h1>
                        <p>Pendaftaran</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-list"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="small-box bg-color-3" style="color: white;">
                    <div class="inner">
                        <h1><?= $question ?></h1>
                        <p>Pertanyaan</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="small-box bg-color-8" style="color: white;">
                    <div class="inner">
                        <h1><?= $cert ?></h1>
                        <p>Pengajuan Sertifikat</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-certificate"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>