<?php

use app\models\MCOntent;
use app\models\TQuestion;
use app\models\TRegistration;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\MContentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Data Quiz';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- DataTables CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<!-- DataTables Buttons CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<!-- jQuery -->
<!-- DataTables JS -->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<!-- DataTables Buttons JS -->
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>


<div class="main-content">
    <div class="container mb-5">
        <div class="card bg-white  shadow">
            
            <div class="card-body">
            <div class="card-title">
            Jawaban Quiz
            </div>
            <table id="example" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>No. HP</th>
                            <th>Jawaban</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataProvider as $key => $model) : ?>
                            <tr>
                                <td><?= $model->created_at ?></td>
                                <td><?= decrypt($model->nama_lengkap) ?></td>
                                <td><?= decrypt($model->email) ?></td>
                                <td><?= decrypt($model->no_hp) ?></td>
                                <td><?= $model->jawaban ?></td>
                               
                            </tr>
                        <?php endforeach; ?>

                        <!-- Tambahkan data lainnya di sini -->
                    </tbody>
                </table>
               
            </div>

        </div>
    </div>
    <div class="modal" id="modal-detail" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Pertanyaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-y: auto;">

                </div>

            </div>
        </div>
    </div>

    <script>
         $(document).ready(function() {
            $('#example').DataTable({
                dom: 'Bfrtip',
                scrollX: true, // Enable horizontal scrolling
                buttons: [{
                    extend: 'excelHtml5',
                    text: 'Export to Excel',
                    title: 'Data Quiz',
                    exportOptions: {
                        // columns: ':visible'
                        // columns: ':not(:last-child)'
                    }
                }]
            });
        });


    </script>