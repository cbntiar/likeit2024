<?php

use app\models\MCOntent;
use app\models\TCertificateRequest;
use app\models\TQuestion;
use app\models\TRegistration;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\MContentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Data Sertifikat';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- DataTables CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<!-- DataTables Buttons CSS -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<!-- jQuery -->
<!-- DataTables JS -->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<!-- DataTables Buttons JS -->
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>


<div class="main-content">
    <div class="container mb-5">
        <div class="card bg-white  shadow">
            
            <div class="card-body">
                <div class="card-title">
                Request Sertifikat
                </div>
                <table id="example" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>

                            <th>ID</th>
                            <th>Date</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>No. HP</th>
                            <th>Instansi</th>
                            <th>Link Foto</th>
                            <th>File Sertifikat</th>
                            <!-- <th>Foto</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataProvider as $key => $model) : ?>
                            <tr>
                                <td><?= $model->id ?></td>
                                <td><?= $model->created_at ?></td>
                                <td><?= $model->nama_lengkap ?></td>
                                <td><?= $model->email ?></td>
                                <td><?= $model->no_hp ?></td>
                                <td><?= $model->instansi ?></td>
                                <td><a href="<?= $model->image ?>" target="_blank" rel="noopener noreferrer"><?= $model->image ?></a></td>
                                <td><a href="<?= Url::toRoute(['/event/get-certificate', 'e_id' => encrypt($model->id)], true) ?>" target="_blank" rel="noopener noreferrer"><?= Url::toRoute(['/event/get-certificate', 'e_id' => encrypt($model->id)], true) ?></a></td>
                                <!-- <td>
                                <button class="btn btn-success btn-sm" onclick="window.open('<?= $model->image ?>', '_blank')">View</button>
                                </td> -->
                                
                            </tr>
                        <?php endforeach; ?>

                        <!-- Tambahkan data lainnya di sini -->
                    </tbody>
                </table>

            </div>

        </div>
    </div>

    <script>

$(document).ready(function() {
            $('#example').DataTable({
                dom: 'Bfrtip',
                scrollX: true, // Enable horizontal scrolling
                buttons: [{
                    extend: 'excelHtml5',
                    text: 'Export to Excel',
                    title: 'Data Request Sertifikat',
                    exportOptions: {
                        // columns: ':visible'
                        // columns: ':not(:last-child)'
                    }
                }]
            });
        });

    </script>