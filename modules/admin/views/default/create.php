<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */

$this->title = 'Create Content';

?>

<div class="main-content">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class="">Create Content</h1>
            </div>
            <div class="card-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>

    </div>
</div>

