<?php

use app\components\Helpers;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Request Sertifikat';

$model->event_id = 1;

?>

<div class="main-content">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class=""><i class="fas fa-certificate"></i> Kirim Sertifikat</h1>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'cert-form'
                ]); ?>
                <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user')])->textInput(['maxlength' => true, 'class' => 'form-control nlb']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input']) ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group field-tcertificaterequest-image">

                            <label class="control-label" for="tcertificaterequest-image">File Sertifikat</label>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-image" aria-hidden="true"></i></span>
                                </div>
                                <input type="file" id="tcertificaterequest-image" class="form-control nlb" name="TCertificateRequest[file]">
                            </div>
                            <div class="help-block"></div>


                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <div class="form-group text-center">
                            <div style="width: 200px;margin:auto;">
                                <?= Html::button('Kirim Sertifikat', ['class' => 'btn btn-success btn-lg', 'id' => 'submit-form']) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submit-form').click(function() {

        if (!isEmail($('#email-input').val())) {
            alert('Email tidak valid')
            return false
        }

        var formData = new FormData($('#cert-form')[0]);

        $.ajax({
            url: `<?= Url::to(['/admin/default/send-certificate-request']) ?>`,
            type: 'POST',
            dataType: 'JSON',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('#submit-form').attr('disabled', true)
            },
            success: function(response) {
                alert(response.message)
                // $('#cert-form')[0].reset()
            },
            complete: function() {
                $('#modal-detail').modal('hide');
                $('#submit-form').attr('disabled', false)

            },
            error: function(xhr, ajaxOptions, thrownError) {
                var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                $('#modal-detail .modal-body').html(pesan);
                $('#submit-form').attr('disabled', false)

                // alert(pesan);
            }
        });
    })

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>