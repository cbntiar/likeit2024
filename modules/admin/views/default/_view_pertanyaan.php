<?php

use app\components\Helpers;
use app\models\MEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */
/** @var yii\widgets\ActiveForm $form */

// $items = ArrayHelper::map(MEvent::find()->all(), 'id', 'event_name');


?>

<div class="tregistration-form">

    <div>
        <?php $form = ActiveForm::begin([
            'id' => 'question-form'
        ]); ?>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input', 'disabled' => true]) ?>
                <?= $form->field($model, 'instansi', ['template' => Helpers::inputIcon('building')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>

            </div>
            <div class="col-md-7">
            <?= $form->field($model, 'created_at', ['template' => Helpers::inputIcon('calendar')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                <?= $form->field($model, 'pertanyaan')->textarea(['maxlength' => true, 'rows' => 6, 'disabled' => true]) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>