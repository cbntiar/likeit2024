<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MCOntent $model */

$this->title = 'Update Mc Ontent: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Mc Ontents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="main-content">
    <div class="container mb-5">
        <div class="card ">
            <div class="card-header">
                <h1 class="">Create Content</h1>
            </div>
            <div class="card-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>

    </div>
</div>