<?php

use app\models\MCOntent;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\MContentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Content Management';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-content">
    <div class="container mb-5">
        <div class="card bg-white">
            <div class="card-header">
                <h1 class="">Content Management</h1>
            </div>
            <div class="card-body">
                <div class="text-right">
                    <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
                </div>
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); 
                ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table  table-sm table-bordered'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        'title',
                        'type',
                        // 'content:ntext',
                        'created_at',
                        //'created_by',
                        'updated_at',
                        //'updated_by',
                        //'views',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, MCOntent $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'id' => $model->id]);
                            }
                        ],
                    ],
                ]); ?>

                <?php Pjax::end(); ?>
            </div>

        </div>
    </div>