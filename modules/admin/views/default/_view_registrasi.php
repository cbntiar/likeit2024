<?php

use app\components\Helpers;
use app\models\MEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\TRegistration $model */
/** @var yii\widgets\ActiveForm $form */

// $items = ArrayHelper::map(MEvent::find()->all(), 'id', 'event_name');

$model->event_id = 1;


?>

<div class="tregistration-form">

    <div>
        <?php $form = ActiveForm::begin([
            'id' => 'registrasi-form'
        ]); ?>
        <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>
        <div class="row">
            <div class="col-md-12">
                <div style="text-align: justify;border: 2px solid #0a4f92 !important;border-radius: 5px;font-size: 18px;" class="p-3 border mb-3">
                    Peserta mendaftar pada: <?= $model->created_at ?></div>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'nama_lengkap', ['template' => Helpers::inputIcon('user')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                <?= $form->field($model, 'no_ktp', ['template' => Helpers::inputIcon('id-card')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'ktp-input', 'disabled' => true]) ?>
                <?= $form->field($model, 'email', ['template' => Helpers::inputIcon('at')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'email-input', 'disabled' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'profesi', ['template' => Helpers::inputIcon('briefcase')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                <?= $form->field($model, 'instansi', ['template' => Helpers::inputIcon('building')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'disabled' => true]) ?>
                <?= $form->field($model, 'no_hp', ['template' => Helpers::inputIcon('phone')])->textInput(['maxlength' => true, 'class' => 'form-control nlb', 'id' => 'no_hp', 'disabled' => true]) ?>
            </div>
            <div class="col-md-12 text-center">
                <?= $form->field($model, 'description')->textarea(['rows' => 6, 'disabled' => true]) ?>
            </div>
            <div class="col-md-12 text-center">
                <?php if ($model->status == 1) : ?>
                    <div class="mb-1">Peserta terpilih untuk mendapatkan undangan email</div>
                    <button class="btn btn-danger btn-lg" onclick="pilihPeserta('<?= $model->id ?>', 0)">Batalkan</button>
                <?php else : ?>
                    <div class="mb-1">Undang acara offline?</div>
                    <button class="btn btn-success btn-lg" onclick="pilihPeserta('<?= $model->id ?>', 1)">Pilih</button>
                <?php endif; ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>